package com.campus.android.app.presenters

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.presenter.InjectPresenter
import com.campus.android.R
import com.campus.android.app.views.CardView
import com.campus.android.domain.converters.CardsConverter
import com.campus.android.domain.models.Card
import com.campus.android.domain.repositories.CardsRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.util.*

/**
 * Created by Nugaev Marat on 16.08.2018.
 * Presenter Card Fragment
 */
@InjectViewState
class CardsPresenter(private val cardsRepository: CardsRepository,
                     private val cardsConverter: CardsConverter): MvpPresenter<CardView>() {
    private val TAG = CardsPresenter::class.java.simpleName

    fun fetchCards(token: String, id: Int) {
        viewState.startLoading()
        cardsRepository.fetchCardsFromDB()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.e(TAG, "presenter cards $it")

                    if (it.isNotEmpty()) {
                        viewState.setupData(data = it)
                        viewState.endLoading()
                    }

                    fetchCardsFromApi(token = token, id = id)
                }, {
                    fetchCardsFromApi(token = token, id = id)
                })
    }

    private fun fetchCardsFromApi(token: String, id: Int) {
        cardsRepository.fetchCards(id = id, token = token)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    viewState.endLoading()
                    if (response.isEmpty()) {
                        viewState.showNoItems()
                    } else {
                        cardsRepository.updateStorage(response)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    Log.e(TAG, "success $it")
                                }, { error ->
                                    Log.e(TAG, "error update cards $error")
                                })

                        val converted = response.map {  cardsConverter.apiToModel(apiCard = it) }
                        viewState.setupData(data = converted)
                    }
                }, { error ->
                    viewState.endLoading()
                    if (error is HttpException) {
                        val responseBody = error.response().errorBody()
                        viewState.showError(message = responseBody?.string().orEmpty())
                    } else {
                        viewState.showError(message = R.string.error_card_delete)
                    }
                })
    }

    fun deleteCard(id: Int, token: String) {
        cardsRepository.deleteCard(token = token, id = id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ _ ->
                    viewState.successDeleted()
                }, { error ->
                    if (error is HttpException) {
                        val responseBody = error.response().errorBody()
                        viewState.showError(message = responseBody?.string().orEmpty())
                    } else {
                        viewState.showError(message = R.string.error_card_delete)
                    }
                })
    }
}