package com.campus.android.app.services

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder

/**
 * Created by agladkov on 12.04.18.
 */
class BService : Service() {
    private val mBinder = LocalBinder()

    inner class LocalBinder : Binder() {
        internal val service: BService
            get() = this@BService
    }

    override fun onCreate() {
        for (x in 0..99) {

        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onBind(p0: Intent?): IBinder {
        return mBinder
    }
}