package com.campus.android.app.activities

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.bumptech.glide.Glide
import com.campus.android.R
import com.campus.android.app.fragments.containers.RegisterContainer
import com.campus.android.app.interfaces.BackButtonListener
import com.campus.android.app.interfaces.RouterProvider
import com.campus.android.base.BaseActivity
import com.campus.android.di.App
import com.campus.android.helpers.ImageHelper
import com.campus.android.helpers.Keys
import com.campus.android.helpers.ScreenKeys
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import pl.aprilapps.easyphotopicker.EasyImage
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.Back
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace
import ru.terrakok.cicerone.commands.SystemMessage
import javax.inject.Inject
import pl.aprilapps.easyphotopicker.DefaultCallback
import java.io.File


class RegisterActivity: BaseActivity(), RouterProvider {

    // Injects
    @Inject lateinit var routerRegister: Router
    @Inject lateinit var navigatorHolder: NavigatorHolder

    private var registerContainer: RegisterContainer? = null

    private fun applyCommand(command: Command) {
        when (command) {
            is Back -> finish()
            is SystemMessage -> Toast.makeText(applicationContext, command.message, Toast.LENGTH_SHORT).show()
            is Replace -> {
                when (command.screenKey) {
                    ScreenKeys.RegisterData.value -> {
                        supportFragmentManager.beginTransaction()
                                .attach(registerContainer)
                                .commitNow()
                    }

                    ScreenKeys.Image.value -> {
                        EasyImage.openCamera(this@RegisterActivity, 500)
                    }

                    ScreenKeys.Calendar.value -> {
                        val intent = Intent(applicationContext, CalendarActivity::class.java)
                        startActivityForResult(intent, 600)
                        overridePendingTransition(R.anim.slide_in_up, R.anim.hold_position)
                    }

                    ScreenKeys.Main.value -> {
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        overridePendingTransition(R.anim.slide_in_up, R.anim.hold_position)
                        finish()
                    }
                }
            }
        }
    }

    private val navigator = Navigator { command ->
        applyCommand(command = command)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(activity = this@RegisterActivity)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initContainers()

        Glide.with(applicationContext)
                .asGif()
                .load(R.raw.animate_login)
                .into(imgRegisterBackground)

        if (savedInstanceState == null) {
            navigator.applyCommand(Replace(ScreenKeys.RegisterData.value, 0))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                //Some error handling
            }

            override fun onImagesPicked(imagesFiles: List<File>, source: EasyImage.ImageSource, type: Int) {
                //Handle the images
                if (imagesFiles.isEmpty()) {
                    registerContainer?.getRouter()?.navigateTo(ScreenKeys.RegisterPassword.value)
                } else {
                    Toast.makeText(applicationContext, getString(R.string.start_saving), Toast.LENGTH_LONG).show()
                    val bmOptions = BitmapFactory.Options()
                    val sourceBitmap = BitmapFactory.decodeFile(imagesFiles[0].absolutePath, bmOptions)
                    val bitmap: Bitmap = ImageHelper.rotateImageIfRequired(applicationContext, sourceBitmap, Uri.fromFile(imagesFiles[0]))
                    ImageHelper.savebitmap(applicationContext, "testfile.png", bitmap, object: ImageHelper.Callback {
                        override fun onSuccess(f: File) {
                            registerContainer?.getRouter()?.navigateTo(ScreenKeys.RegisterPassword.value, f)
                        }

                        override fun onError(error: java.lang.Exception) {
                            Toast.makeText(applicationContext, getString(R.string.error_upload) + " " +
                                    error.localizedMessage, Toast.LENGTH_LONG).show()
                        }
                    })
                }
            }
        })
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.flRegisterContainer)
        if (fragment != null && fragment is BackButtonListener && fragment.onBackPressed()) {
            return
        } else {
            applyCommand(Back())
        }
    }

    private fun initContainers() {
        registerContainer = supportFragmentManager.findFragmentByTag(ScreenKeys.RegisterData.value) as? RegisterContainer
        if (registerContainer == null) {
            registerContainer = RegisterContainer.getNewInstance(name = ScreenKeys.RegisterData.value)
            supportFragmentManager.beginTransaction()
                    .add(R.id.flRegisterContainer, registerContainer, ScreenKeys.RegisterData.value)
                    .detach(registerContainer)
                    .commitNow()
        }
    }

    // MARK: - Router provider implementation
    override fun getRouter(): Router {
        return routerRegister
    }
}