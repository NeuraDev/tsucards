package com.campus.android.app.activities

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.util.Log
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.campus.android.R
import com.campus.android.app.AddCardActivity
import com.campus.android.app.fragments.containers.CardsContainer
import com.campus.android.app.fragments.containers.KeysContainer
import com.campus.android.app.fragments.containers.MessagesContainer
import com.campus.android.app.fragments.containers.ProfileContainer
import com.campus.android.app.interfaces.BackButtonListener
import com.campus.android.app.interfaces.ConfigurationListener
import com.campus.android.app.interfaces.RouterProvider
import com.campus.android.app.presenters.MainPresenter
import com.campus.android.app.views.MainView
import com.campus.android.di.App
import com.campus.android.domain.models.Configuration
import com.campus.android.domain.models.Gate
import com.campus.android.domain.repositories.AuthRepository
import com.campus.android.domain.repositories.ReaderRepository
import com.campus.android.helpers.BottomNavigationViewHelper
import com.campus.android.helpers.ImageHelper
import com.campus.android.helpers.Keys
import com.campus.android.helpers.ScreenKeys
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.add_key.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.Back
import ru.terrakok.cicerone.commands.Replace
import ru.terrakok.cicerone.commands.SystemMessage
import java.io.File
import javax.inject.Inject


/**
 * Created by Nugaev Marat on 14.08.2018.
 * Edited by Alex Gladkov on 15.08.2018.
 */
class MainActivity : MvpAppCompatActivity(), MainView, RouterProvider, ConfigurationListener {
    private val TAG: String = MainActivity::class.java.simpleName

    // MARK: - Presenter setup
    @InjectPresenter
    lateinit var mainPresenter: MainPresenter

    @ProvidePresenter
    fun provideMainPresenter(): MainPresenter {
        return MainPresenter(router = routerMain, authRepository = authRepository,
                readerRepository = readerRepository)
    }

    // Injects
    @Inject lateinit var authRepository: AuthRepository
    @Inject lateinit var readerRepository: ReaderRepository
    @Inject lateinit var navigatorHolder: NavigatorHolder
    @Inject lateinit var routerMain: Router

    private var cardsContainer: CardsContainer? = null
    private var keysContainer: KeysContainer? = null
    private var messagesContainer: MessagesContainer? = null
    private var profileContainer: ProfileContainer? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_key -> {
                mainPresenter.switchFragment(case = 0)
            }
            R.id.navigation_message -> {
                mainPresenter.switchFragment(case = 1)
            }
            R.id.navigation_cards -> {
                mainPresenter.switchFragment(case = 2)
            }
            R.id.navigation_profile -> {
                mainPresenter.switchFragment(case = 3)
            }
        }

        return@OnNavigationItemSelectedListener true
    }

    private val navigator = Navigator { command ->
        when (command) {
            is Back -> finish()
            is SystemMessage -> Toast.makeText(applicationContext, command.message, Toast.LENGTH_SHORT).show()
            is Replace -> {
                when (command.screenKey) {
                    ScreenKeys.AddKey.value -> {
                        startActivity(Intent(applicationContext, AddKeyActivity::class.java))
                        overridePendingTransition(R.anim.slide_in_up, R.anim.hold_position)
                    }

                    ScreenKeys.AddCard.value ->{
                        val addCardIntent = Intent(applicationContext, AddCardActivity::class.java)
                        addCardIntent.putExtra(Keys.Empty.value, command.transitionData as? Boolean)
                        startActivityForResult(addCardIntent, 100)
                        overridePendingTransition(R.anim.slide_in_up, R.anim.hold_position)
                    }


                    ScreenKeys.Url.value -> {
                        val url = command.transitionData as? String ?: ""
                        if (url != "") {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                        }
                    }

                    ScreenKeys.Login.value -> {
                        startActivity(Intent(applicationContext, SplashActivity::class.java))
                        overridePendingTransition(R.anim.slide_in_up, R.anim.hold_position)
                        finish()
                    }

                    ScreenKeys.Calendar.value -> {
                        val intent = Intent(applicationContext, CalendarActivity::class.java)
                        intent.putExtra(Keys.Calendar.value, command.transitionData as? String)
                        startActivityForResult(intent, 600)
                        overridePendingTransition(R.anim.slide_in_up, R.anim.hold_position)
                    }

                    ScreenKeys.Image.value -> {
                        EasyImage.openCamera(this@MainActivity, 500)
                    }

                    ScreenKeys.Keys.value -> {
                        supportFragmentManager.beginTransaction()
                                .detach(cardsContainer)
                                .detach(messagesContainer)
                                .detach(profileContainer)
                                .attach(keysContainer)
                                .commitNow()
                    }

                    ScreenKeys.Messages.value -> {
                        supportFragmentManager.beginTransaction()
                                .detach(cardsContainer)
                                .detach(keysContainer)
                                .detach(profileContainer)
                                .attach(messagesContainer)
                                .commitNow()
                    }

                    ScreenKeys.Cards.value -> {
                        supportFragmentManager.beginTransaction()
                                .detach(keysContainer)
                                .detach(messagesContainer)
                                .detach(profileContainer)
                                .attach(cardsContainer)
                                .commitNow()
                    }

                    ScreenKeys.Profile.value -> {
                        supportFragmentManager.beginTransaction()
                                .detach(keysContainer)
                                .detach(messagesContainer)
                                .detach(cardsContainer)
                                .attach(profileContainer)
                                .commitNow()
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(activity = this@MainActivity)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initContainers()

        // Bottom bar customization
        BottomNavigationViewHelper.removeShiftMode(navigation)

        val sharedPreferences = getSharedPreferences(getString(R.string.app_name), 0)
        mainPresenter.fetchConfiguration(hasCards = sharedPreferences.getBoolean(Keys.Cards.value, false))
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            600 -> {
                val selectedDate = data?.extras?.get(Keys.Calendar.value) as? String
                profileContainer?.updateDate(selectedDate = selectedDate)
            }
            100 -> {
                navigation.selectedItemId = R.id.navigation_key
            }
            else -> EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
                override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                    //Some error handling
                }

                override fun onImagesPicked(imagesFiles: List<File>, source: EasyImage.ImageSource, type: Int) {
                    //Handle the images
                    if (imagesFiles.isNotEmpty()) {
                        Toast.makeText(applicationContext, getString(R.string.start_saving), Toast.LENGTH_LONG).show()
                        val bmOptions = BitmapFactory.Options()
                        val sourceBitmap = BitmapFactory.decodeFile(imagesFiles[0].absolutePath, bmOptions)
                        val bitmap: Bitmap = ImageHelper.rotateImageIfRequired(applicationContext, sourceBitmap, Uri.fromFile(imagesFiles[0]))
                        ImageHelper.savebitmap(applicationContext, "testfile.png", bitmap, object: ImageHelper.Callback {
                            override fun onSuccess(f: File) {
                                Toast.makeText(applicationContext, getString(R.string.start_upload), Toast.LENGTH_LONG).show()
                                profileContainer?.uploadAvatar(image = f)
                            }

                            override fun onError(error: java.lang.Exception) {
                                Toast.makeText(applicationContext, getString(R.string.error_upload) + " " +
                                        error.localizedMessage, Toast.LENGTH_LONG).show()
                            }
                        })
                    }
                }
            })
        }
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.flMainContainer)
        if (fragment != null && fragment is BackButtonListener && fragment.onBackPressed()) {
            return
        } else {
            mainPresenter.onBackPressed()
        }
    }

    private fun initContainers() {
        keysContainer = supportFragmentManager.findFragmentByTag(ScreenKeys.Keys.value) as? KeysContainer
        if (keysContainer == null) {
            keysContainer = KeysContainer.getNewInstance(name = ScreenKeys.Keys.value)
            supportFragmentManager.beginTransaction()
                    .add(R.id.flMainContainer, keysContainer, ScreenKeys.Keys.value)
                    .detach(keysContainer)
                    .commitNow()
        }

        messagesContainer = supportFragmentManager.findFragmentByTag(ScreenKeys.Messages.value) as? MessagesContainer
        if (messagesContainer == null) {
            messagesContainer = MessagesContainer.getNewInstance(name = ScreenKeys.Messages.value)
            supportFragmentManager.beginTransaction()
                    .add(R.id.flMainContainer, messagesContainer, ScreenKeys.Messages.value)
                    .detach(messagesContainer)
                    .commitNow()
        }

        cardsContainer = supportFragmentManager.findFragmentByTag(ScreenKeys.Cards.value) as? CardsContainer
        if (cardsContainer == null) {
            cardsContainer = CardsContainer.getNewInstance(name = ScreenKeys.Cards.value)
            supportFragmentManager.beginTransaction()
                    .add(R.id.flMainContainer, cardsContainer, ScreenKeys.Cards.value)
                    .detach(cardsContainer)
                    .commitNow()
        }

        profileContainer = supportFragmentManager.findFragmentByTag(ScreenKeys.Profile.value) as? ProfileContainer
        if (profileContainer == null) {
            profileContainer = ProfileContainer.getNewInstance(name = ScreenKeys.Profile.value)
            supportFragmentManager.beginTransaction()
                    .add(R.id.flMainContainer, profileContainer, ScreenKeys.Profile.value)
                    .detach(profileContainer)
                    .commitNow()
        }
    }

    // MARK: - Router provider implementation
    override fun getRouter(): Router {
        return routerMain
    }

    // MARK: - Configuration listener implementation
    override fun provideConfiguration(): Configuration {
        return mainPresenter.provideConfiguration()
    }

    override fun updateSelfConfiguration() {
        // Under construction
    }

    // MARK: - View implementation
    override fun configurationLoaded(index: Int) {
        when (index) {
            2 -> {
                routerMain.replaceScreen(ScreenKeys.Cards.value)
                navigation.selectedItemId = R.id.navigation_cards
            }

            else -> {
                routerMain.replaceScreen(ScreenKeys.Messages.value)
                navigation.selectedItemId = R.id.navigation_message
            }
        }

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }
}
