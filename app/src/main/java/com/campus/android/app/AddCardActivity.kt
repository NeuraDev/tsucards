package com.campus.android.app

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.campus.android.R
import com.campus.android.R.id.btnAddCardSave
import com.campus.android.app.presenters.AddCardPresenter
import com.campus.android.app.views.AddCardView
import com.campus.android.di.App
import com.campus.android.domain.repositories.AuthRepository
import com.campus.android.domain.repositories.CardsRepository
import com.campus.android.helpers.Keys
import com.campus.android.helpers.WindowConfig
import com.campus.android.helpers.WindowUtils
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.add_card.*
import javax.inject.Inject

/**
 * Created by Nugaev Marat on 14.08.2018.
 * Modified by Alex Gladkov on 20.08.2018
 */

class AddCardActivity : MvpAppCompatActivity(), AddCardView {

    // MARK: - Injects
    @Inject lateinit var cardsRepository: CardsRepository
    @Inject lateinit var authRepository: AuthRepository

    // MARK: - Presenter setup
    @InjectPresenter
    lateinit var addCardPresenter: AddCardPresenter

    @ProvidePresenter
    fun provideAddCardPresenter(): AddCardPresenter {
        return AddCardPresenter(cardsRepository = cardsRepository)
    }

    lateinit var inputLayoutCardNumber: TextInputLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(activity = this@AddCardActivity)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_card)
        inputLayoutCardNumber = findViewById(R.id.inputLayoutCardNumber)

        textAddCardNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val value = textAddCardNumber.text.replace(Regex.fromLiteral("[^\\d]"), "")
                val TAG = AddCardActivity::class.java.simpleName

                when {
                    value.isNotEmpty() && value.startsWith("4", ignoreCase = false) -> {
                        textAddCardNumber.setPadding(WindowUtils.convertDpToPixel(48f, applicationContext)
                                .toInt(), 0, 0, 0)
                        imgAddCardPreview.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ic_card_visa))
                        imgAddCardPreview.visibility = View.VISIBLE
                    }
                    value.length >= 2 && value.substring(0, 2).toInt() in 51..57 -> {
                        textAddCardNumber.setPadding(WindowUtils.convertDpToPixel(48f, applicationContext)
                                .toInt(), 0, 0, 0)
                        imgAddCardPreview.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.mipmap.mastercard))
                        imgAddCardPreview.visibility = View.VISIBLE
                    }

                    value.length >= 4 && value.substring(0, 4).toInt() in 2221..2720 -> {
                        textAddCardNumber.setPadding(WindowUtils.convertDpToPixel(48f, applicationContext)
                                .toInt(), 0, 0, 0)
                        imgAddCardPreview.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.mipmap.mastercard))
                        imgAddCardPreview.visibility = View.VISIBLE
                    }

                    value.length >= 4 && value.substring(0, 4).toInt() in 2200..2204 -> {
                        textAddCardNumber.setPadding(WindowUtils.convertDpToPixel(48f, applicationContext)
                                .toInt(), 0, 0, 0)
                        imgAddCardPreview.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.mipmap.ic_mir_logo))
                        imgAddCardPreview.visibility = View.VISIBLE
                    }
                    else -> {
                        textAddCardNumber.setPadding(0, 0, 0, 0)
                        imgAddCardPreview.visibility = View.GONE
                    }
                }
            }
        })

        btnAddCardBack.setOnClickListener {
            onBackPressed()
        }

        btnAddCardSave.setOnClickListener {
            if (textAddCardNumber.text.toString().isEmpty() || textAddCardMonth.text.toString().isEmpty()
                    || textAddCardYear.text.toString().isEmpty()) {
                Toast.makeText(applicationContext, R.string.please_field, Toast.LENGTH_LONG).show()
            } else {
                authRepository.fetchConfiguration()
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ configuration ->
                            addCardPresenter.addCard(token = configuration.token,
                                    userId = configuration.id.toInt(), month = textAddCardMonth.text.toString(),
                                    year = textAddCardYear.text.toString(),
                                    cardNumber = textAddCardNumber.text.toString().replace(" ", ""))
                        })
            }
        }
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.hold_position, R.anim.slide_down)
    }

    // MARK: - View implementation
    override fun startSending() {
        btnAddCardSave.visibility = View.GONE
        textAddCardNumber.isEnabled = false
        textAddCardMonth.isEnabled = false
        textAddCardYear.isEnabled = false
    }

    override fun endSending() {
        btnAddCardSave.visibility = View.VISIBLE
        textAddCardNumber.isEnabled = true
        textAddCardMonth.isEnabled = true
        textAddCardYear.isEnabled = true
    }

    override fun showError(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
    }

    override fun showError(message: Int) {
        Toast.makeText(applicationContext, getString(message), Toast.LENGTH_LONG).show()
    }

    override fun showSuccess() {
        if (intent?.extras?.getBoolean(Keys.Empty.value) == true) {
            val data = Intent()
            setResult(RESULT_OK, data)
        }

        Toast.makeText(applicationContext, getString(R.string.add_card_success), Toast.LENGTH_LONG).show()
        onBackPressed()
    }
}
