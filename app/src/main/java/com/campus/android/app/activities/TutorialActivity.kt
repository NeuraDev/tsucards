package com.campus.android.app.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.asksira.loopingviewpager.LoopingViewPager
import com.campus.android.R
import com.campus.android.app.adapters.TutorialAdapter
import com.campus.android.helpers.Keys
import kotlinx.android.synthetic.main.activity_tutorial.*

class TutorialActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)

        val dataList: ArrayList<Int> = ArrayList()
        dataList.add(0)
        dataList.add(1)
        dataList.add(2)
        dataList.add(3)
        dataList.add(4)
        vpTutorial.adapter = TutorialAdapter(this@TutorialActivity, dataList)
        pivTutorial.count = vpTutorial.indicatorCount

        fabTutorialNext.setOnClickListener {
            if (vpTutorial.currentItem == 4) {
                getSharedPreferences(getString(R.string.app_name), 0)
                        .edit().putBoolean(Keys.Tutorial.value, true).apply()
                startActivity(Intent(applicationContext, LoginActivity::class.java))
            } else {
                vpTutorial.currentItem = vpTutorial.currentItem + 1
            }
        }

        vpTutorial.setIndicatorPageChangeListener(object: LoopingViewPager.IndicatorPageChangeListener {
            override fun onIndicatorProgress(selectingPosition: Int, progress: Float) {

            }

            override fun onIndicatorPageChange(newIndicatorPosition: Int) {
                pivTutorial.setSelected(newIndicatorPosition)
            }
        })
    }
}