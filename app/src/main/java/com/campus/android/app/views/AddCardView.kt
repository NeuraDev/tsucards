package com.campus.android.app.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Alex Gladkov on 20.08.2018.
 * View for AddCardActivity
 */
@StateStrategyType(value = OneExecutionStateStrategy::class)
interface AddCardView: MvpView {
    fun startSending()
    fun endSending()
    fun showError(message: String)
    fun showError(message: Int)
    fun showSuccess()
}