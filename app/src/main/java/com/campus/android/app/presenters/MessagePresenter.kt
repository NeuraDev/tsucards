package com.campus.android.app.presenters

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.google.firebase.analytics.FirebaseAnalytics
import com.campus.android.app.views.CardView
import com.campus.android.app.views.MessageView
import com.campus.android.domain.models.Card
import com.campus.android.domain.models.Message
import com.campus.android.domain.repositories.MessagesRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * Created by Nugaev Marat on 16.08.2018.
 * Presenter Message Fragment
 */
@InjectViewState
class MessagePresenter(val messagesRepository: MessagesRepository): MvpPresenter<MessageView>() {
    private val TAG = MessagePresenter::class.java.simpleName

    fun fetchMessage(token: String, id: Int) {
        viewState.startLoading()
        messagesRepository.fetchMessages()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    if (response.isNotEmpty()) {
                        viewState.endLoading()
                        viewState.setupData(data = response)
                    }

                    fetchRemoteMessages(token = token, id = id)
                }, {
                    fetchRemoteMessages(token = token, id = id)
                })
    }

    private fun fetchRemoteMessages(token: String, id: Int) {
        messagesRepository.fetchRemoteMessages(id = id, token = token)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    viewState.endLoading()
                    if (response.isEmpty()) {
                        viewState.showNoItems()
                    } else {
                        messagesRepository.updateStorage(newItems = response)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(Schedulers.newThread())
                                .subscribe({
                                    Log.e(TAG, "messages successfully updated")
                                }, { error ->
                                    Log.e(TAG, "error updating messages $error")
                                })

                        viewState.setupData(data = response)
                    }
                }, { error ->
                    viewState.endLoading()
                    viewState.showError(message = error.localizedMessage)
                })
    }
}
