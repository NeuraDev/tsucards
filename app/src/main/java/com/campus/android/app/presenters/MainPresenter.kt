package com.campus.android.app.presenters

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.campus.android.app.views.MainView
import com.campus.android.domain.models.Configuration
import com.campus.android.domain.repositories.AuthRepository
import com.campus.android.domain.repositories.ReaderRepository
import com.campus.android.helpers.ScreenKeys
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router

/**
 * Created by Alex Gladkov on 15.08.18.
 * Presenter for Main Activity
 */
@InjectViewState
class MainPresenter(val router: Router, val authRepository: AuthRepository,
                    val readerRepository: ReaderRepository): MvpPresenter<MainView>() {
    private val TAG: String = MainPresenter::class.java.simpleName
    private val badges = arrayListOf(0, 0, 0, 0, 0)
    private var lastTab = 0
    private var configuration = Configuration.defaultInstance()

    fun fetchConfiguration(hasCards: Boolean) {
        authRepository.fetchConfiguration()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { configuration ->
                    authRepository.sendFcm(fcmToken = configuration.fcmToken.orEmpty(), token = configuration.token)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                Log.e(TAG, "fcmToken send success ${configuration.fcmToken}")
                            }, { error ->
                                Log.e(TAG, "fcmToken send error $error")
                            })

                    this.configuration = configuration

                    if (hasCards) {
                        viewState.configurationLoaded(index = 0)
                    } else {
                        viewState.configurationLoaded(index = 2)
                    }
                }
    }

    fun provideConfiguration(): Configuration {
        return configuration

    }

    fun switchFragment(case: Int) {
        when (case) {
            0 -> router.replaceScreen(ScreenKeys.Keys.value)
            1 -> router.replaceScreen(ScreenKeys.Messages.value)
            2 -> router.replaceScreen(ScreenKeys.Cards.value)
            3 -> router.replaceScreen(ScreenKeys.Profile.value)
        }

//        if (lastTab == case) viewState.performDoubleTap(case = lastTab)
        lastTab = case
    }

//    fun incBadge(position: Int) {
//        if (position > badges.size) return
//        badges[position] += 1
//        viewState.updateBadge(badgeValue = badges[position], position = position)
//    }
//
//    fun clearBadge(position: Int) {
//        if (position > badges.size) return
//        badges[position] = 0
//        viewState.updateBadge(badgeValue = badges[position], position = position)
//    }

    fun onBackPressed() {
//        router.exit()
    }

//    fun handlePush(extras: Bundle?) {
//        val type = extras?.get(EnumCollections.Params.Type.value) as? String ?: ""
//        val data = extras?.get(EnumCollections.Params.Data.value) as? String ?: ""
//
//        when (type) {
//            EnumCollections.Push.Chat.value -> viewState.setTabItem(case = 4)
//            else -> viewState.setTabItem(case = 0)
//        }
//    }
}
