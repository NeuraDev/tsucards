package com.campus.android.app.interfaces

/**
 * Created by agladkov on 13.02.18.
 * This interface provides abstract navigation from fragments to activity
 * is it need to be
 *
 * This works by passing key of CURRENT! screen key to navigation listener
 * Based on this variable listener will call router.navigateTo
 * @see ru.terrakok.cicerone.Router
 */
interface NavigationListener {
    fun nextClick(key: String)
}