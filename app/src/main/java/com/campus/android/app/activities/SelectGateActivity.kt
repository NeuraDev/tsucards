package com.campus.android.app.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.campus.android.R
import com.campus.android.app.adapters.GatesAdapter
import com.campus.android.base.BaseAdapterCallback
import com.campus.android.data.implementations.ReadersRepositoryImpl
import com.campus.android.di.App
import com.campus.android.domain.models.Gate
import com.campus.android.domain.repositories.AuthRepository
import com.campus.android.domain.repositories.ReaderRepository
import com.campus.android.helpers.Keys
import com.campus.android.helpers.ListConfig
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_select_gate.*
import javax.inject.Inject

class SelectGateActivity : MvpAppCompatActivity() {
    private val TAG = SelectGateActivity::class.java.simpleName

    // MARK: - UI
    private val mAdapter = GatesAdapter()

    // MARK: - Injects
    @Inject
    lateinit var readersRepository: ReaderRepository
    @Inject
    lateinit var authRepository: AuthRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(activity = this@SelectGateActivity)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_gate)

        authRepository.fetchConfiguration()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ configuration ->
                    readersRepository.fetchReaders(id = configuration.id.toInt(), token = configuration.token)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                mAdapter.setList(dataList = it)
                            }, {
                                Toast.makeText(applicationContext, it.localizedMessage, Toast.LENGTH_LONG).show()
                                onBackPressed()
                            })
                }, { error ->
                    Toast.makeText(applicationContext, error.localizedMessage, Toast.LENGTH_LONG).show()
                    onBackPressed()
                })

        mAdapter.attachCallback(object : BaseAdapterCallback<Gate> {
            override fun onItemClick(model: Gate, view: View) {
                val data = Intent()

                data.putExtra(Keys.Gate.value, model)
                setResult(RESULT_OK, data)
                onBackPressed()
            }

            override fun onLongClick(model: Gate, view: View): Boolean {
                return true
            }
        })

        val listConfig = ListConfig.Builder(mAdapter)
                .setHasFixedSize(isFixedSize = true)
                .setHasNestedScroll(isNestedScroll = true)
                .build(applicationContext)

        listConfig.applyConfig(context = applicationContext, recyclerView = recyclerGates)
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.hold_position, R.anim.slide_down)
    }
}