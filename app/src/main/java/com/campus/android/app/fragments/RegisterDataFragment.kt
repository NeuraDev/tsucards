package com.campus.android.app.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.campus.android.R
import com.campus.android.app.adapters.CardsAdapter
import com.campus.android.app.interfaces.RouterProvider
import com.campus.android.app.presenters.CardsPresenter
import com.campus.android.app.views.CardView
import com.campus.android.base.BaseChildFragment
import com.campus.android.data.implementations.AuthRepositoryImpl
import com.campus.android.di.App
import com.campus.android.domain.models.Card
import com.campus.android.domain.repositories.AuthRepository
import com.campus.android.helpers.ListConfig
import com.campus.android.helpers.ScreenKeys
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.add_key.*
import kotlinx.android.synthetic.main.fragment_cards.*
import kotlinx.android.synthetic.main.fragment_register_data.*
import javax.inject.Inject
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.R.id.edit
import android.content.Context
import com.google.android.gms.common.util.InputMethodUtils.showSoftInput
import android.content.Context.INPUT_METHOD_SERVICE
import android.util.Log
import android.view.inputmethod.InputMethodManager


/**
 * A simple [Fragment] subclass.
 */
class RegisterDataFragment: BaseChildFragment() {
    private val TAG = ProfileFragment::class.java.simpleName
    // MARK: - Injects
    @Inject lateinit var authRepository: AuthRepository

    companion object {
        fun getNewInstance(): RegisterDataFragment {
            return RegisterDataFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@RegisterDataFragment)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_register_data, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val statusArray = arrayListOf<String>(getString(R.string.register_status_student),
                getString(R.string.register_status_worker), getString(R.string.register_status_guest))
        spRegisterData.setItems(statusArray)

        btnRegisterDataNext.setOnClickListener {
            if (textRegisterDate.text.toString().isEmpty() || textRegisterSurname.text.toString().isEmpty()
                    || textRegisterName.text.toString().isEmpty() || textRegisterPatronymic.text.toString().isEmpty()) {
                Toast.makeText(context, getString(R.string.error_register_data_empty), Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            authRepository.fetchConfiguration()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { configuration ->
                        configuration.surname = textRegisterSurname.text.toString()
                        configuration.name = textRegisterName.text.toString()
                        configuration.patronymic = textRegisterPatronymic.text.toString()
                        configuration.birthDay = textRegisterDate.text.toString()
                        configuration.status = statusArray[spRegisterData.selectedIndex]

                        authRepository.updateConfiguration(configuration = configuration)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe { _ ->
                                    Log.e(TAG, "bd ${configuration}")
                                    (parentFragment as? RouterProvider)?.getRouter()?.navigateTo(ScreenKeys.ImagePreview.value)
                                }
                    }
        }
    }

    override fun onResume() {
        super.onResume()
        textRegisterSurname.post {
            textRegisterSurname.requestFocus()
            val imgr = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imgr?.showSoftInput(textRegisterSurname, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}
