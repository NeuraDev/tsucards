package com.campus.android.app.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.campus.android.R
import com.campus.android.R.id.textAddCardNumber
import com.campus.android.app.activities.MainActivity
import com.campus.android.base.BaseAdapter
import com.campus.android.base.BaseViewHolder
import com.campus.android.domain.models.Card
import kotlinx.android.synthetic.main.cell_card.view.*

/**
 * Created by Nugaev Marat on 16.08.2018.
 * Adapter for cards fragment
 */
class CardsAdapter : BaseAdapter<Card>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Card> {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_card, parent, false))
    }

    class ViewHolder(itemView: View) : BaseViewHolder<Card>(itemView = itemView) {
        private val TAG: String = ViewHolder::class.java.simpleName
        private var txtTitle: TextView = itemView.findViewById<View>(R.id.txtCardTitle) as TextView
        private var txtValue: TextView = itemView.findViewById<View>(R.id.txtCardValue) as TextView
        private var imgIcon: ImageView = itemView.findViewById<ImageView>(R.id.imageCardIcon) as ImageView
        private var imgCardHolder: ImageView = itemView.findViewById(R.id.imgCardHolder)

        @SuppressLint("SetTextI18n")
        override fun bind(model: Card) {
            when (model.bankName) {
                "sberbank" -> imgCardHolder.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.img_card_sberbank))
                else -> imgCardHolder.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.ic_card_black))
            } 

            when {
                model.title.startsWith("4", ignoreCase = false) -> {
                    imgIcon.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.ic_card_visa))
                }
                model.title.substring(0, 2).toInt() in 51..57 -> {
                    imgIcon.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.mastercard))
                }
                model.title.substring(0, 4).toInt() in 2221..2720 -> {
                    imgIcon.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.mastercard))
                }
                model.title.substring(0, 4).toInt() in 2200..2204 -> {
                    imgIcon.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.ic_mir_logo))
                }
            }

            txtTitle.text = model.title.substring(12, 16)
            txtValue.text = "${model.month}/${model.year}"
        }
    }
}