package com.campus.android.app.presenters

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.campus.android.app.views.KeyView
import com.campus.android.domain.models.Key
import com.campus.android.domain.repositories.KeysRepository
import com.campus.android.domain.repositories.ReaderRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * Created by Alex Gladkov on 15.08.18.
 * Presenter Key Fragment
 */
@InjectViewState
class KeysPresenter(private val keysRepository: KeysRepository,
                    private val readersRepository: ReaderRepository): MvpPresenter<KeyView>() {
    private val TAG = KeysPresenter::class.java.simpleName

    fun fetchKeys(token: String, userId: Int) {
        viewState.startLoading()
        keysRepository.fetchKeys(token = token, userId = userId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    viewState.endLoading()
                    if (response.isEmpty()) {
                        viewState.showNoItems()
                    } else {
                        readersRepository.fetchReaders(id = userId, token = token)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({ gates ->
                                    Log.e(TAG, "gates $gates")
                                    viewState.setupData(data = response
                                            .map { key ->
                                                val gate = gates.firstOrNull { gate -> gate.id == key.gateId }
                                                key.title = "${gate?.title.orEmpty()}, ${gate?.place.orEmpty()}"
                                                key})
                                }, { error ->
                                    viewState.endLoading()
                                    viewState.showError(message = error.localizedMessage)
                                })
                    }
                }, { error ->
                    viewState.endLoading()
                    viewState.showError(message = error.localizedMessage)
                })
    }
}