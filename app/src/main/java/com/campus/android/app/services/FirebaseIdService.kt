package com.campus.android.app.services

import android.os.Handler
import android.util.Log
import com.campus.android.di.App
import com.campus.android.domain.repositories.AuthRepository
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FirebaseIdService: FirebaseInstanceIdService() {
    private val TAG = FirebaseIdService::class.java.simpleName
    private val handler = Handler()

//    @Inject lateinit var otherRepository: OtherRepositoryImpl
//    @Inject lateinit var configurationRepository: ConfigurationRepositoryImpl

    @Inject lateinit var authRepository: AuthRepository

    init {
        App.appComponent.inject(service = this@FirebaseIdService)
    }

    override fun onTokenRefresh() {
        super.onTokenRefresh()
        val refreshedToken = FirebaseInstanceId.getInstance().token
        refreshedToken?.let {
            Log.e(TAG, "refreshed $refreshedToken")
            authRepository.fetchConfiguration()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ configuration ->
                        configuration.fcmToken = it

                        authRepository.updateConfiguration(configuration = configuration)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({ _ ->
                                    if (configuration.token.isNotEmpty()) {
                                        sendFcm(fcmToken = it, token = configuration.token)
                                    }
                                })
                    })
        }
    }

    private fun sendFcm(fcmToken: String, token: String) {
        if (fcmToken == "" || token == "") return
        authRepository.sendFcm(fcmToken = fcmToken, token = token)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.e(TAG, "fcmToken send success $fcmToken")
                }, { error ->
                    Log.e(TAG, "fcmToken send error $error")
                })
    }
}