package com.campus.android.app.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.campus.android.R
import com.campus.android.app.adapters.MessageAdapter
import com.campus.android.app.interfaces.ConfigurationListener
import com.campus.android.app.interfaces.RouterProvider
import com.campus.android.app.presenters.MessagePresenter
import com.campus.android.app.views.MessageView
import com.campus.android.base.BaseChildFragment

import com.campus.android.di.App
import com.campus.android.domain.models.Message
import com.campus.android.domain.repositories.MessagesRepository
import com.campus.android.helpers.ListConfig
import com.campus.android.helpers.ScreenKeys
import kotlinx.android.synthetic.main.fragment_message.*
import org.joda.time.LocalDateTime
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 * Created by Nugaev Marat 16.08.18.
 */
class MessageFragment : BaseChildFragment(), MessageView {
    // MARK: - Injects
    @Inject lateinit var messagesRepository: MessagesRepository

    // MARK: - Presenter setup
    @InjectPresenter
    lateinit var messagePresenter: MessagePresenter

    @ProvidePresenter
    fun provideMessagePresenter(): MessagePresenter {
        return MessagePresenter(messagesRepository = messagesRepository)
    }

    private val mAdapter = MessageAdapter()

    companion object {
        fun getNewInstance(): MessageFragment {
            return MessageFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@MessageFragment)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_message, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let { ctx ->
            val listConfig = ListConfig.Builder(adapter = mAdapter)
                    .setHasFixedSize(isFixedSize = true)
                    .setHasNestedScroll(isNestedScroll = false)
                    .build(context = ctx)
            listConfig.applyConfig(context = ctx, recyclerView = recyclerMessage)
        }

        messagePresenter.fetchMessage(id = (activity as? ConfigurationListener)?.provideConfiguration()?.id?.toInt() ?: -1,
                token = (activity as? ConfigurationListener)?.provideConfiguration()?.token.orEmpty())

    }

    // MARK: - View implementation
    override fun setupData(data: List<com.campus.android.domain.models.Message>) {
        try {
            if (mAdapter.hasItems) {
                mAdapter.updateItems(itemsList = data)
            } else {
                mAdapter.setList(dataList = data)
            }

            recyclerMessage.visibility = View.VISIBLE
            llMessagesNoItems.visibility = View.GONE
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun startLoading() {
        try {
            cpvMessages.visibility = View.VISIBLE
            recyclerMessage.visibility = View.GONE
            llMessagesNoItems.visibility = View.GONE
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun endLoading() {
        try {
            cpvMessages.visibility = View.GONE
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun showError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun showNoItems() {
        try {
            llMessagesNoItems.visibility = View.VISIBLE
            recyclerMessage.visibility = View.GONE
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }
}
