package com.campus.android.app.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.campus.android.R
import com.campus.android.app.presenters.LoginPresenter
import com.campus.android.app.views.LoginView
import com.campus.android.di.App
import com.campus.android.domain.repositories.AuthRepository
import com.google.android.gms.common.util.IOUtils
import kotlinx.android.synthetic.main.activity_login.*
import java.io.IOException
import java.io.InputStream
import javax.inject.Inject
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.ViewTarget


/**
 * Created by Nugaev Marat on 14.08.2018.
 * Modified by Alex Gladkov on 18.08.2018.
 */

class LoginActivity : MvpAppCompatActivity(), LoginView {
    // MARK: - Injects
    @Inject lateinit var authRepository: AuthRepository

    // MARK: - Presenter setup
    @InjectPresenter
    lateinit var loginPresenter: LoginPresenter

    @ProvidePresenter
    fun provideLoginPresenter(): LoginPresenter {
        return LoginPresenter(authRepository = authRepository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(activity = this@LoginActivity)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        Glide.with(applicationContext)
                .asGif()
                .load(R.raw.animate_login)
                .into(imgLoginBackground)

        btnLoginEnter.setOnClickListener {
//            if (textLoginEmail.text.toString().isEmpty() || textLoginPassword.text.toString().isEmpty()){
//                Toast.makeText(applicationContext, R.string.please_field, Toast.LENGTH_LONG).show()
//            }

            loginPresenter.login(email = textLoginEmail.text.toString(),
                    password = textLoginPassword.text.toString())
        }

        btnLoginRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
            overridePendingTransition(R.anim.slide_in_up, R.anim.hold_position)
        }
    }

    // MARK: - View implementation
    override fun showError(message: Int) {
        Toast.makeText(applicationContext, getString(message), Toast.LENGTH_LONG).show()
    }

    override fun showError(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
    }

    override fun startLogin() {
        cpvLogin.visibility = View.VISIBLE
        btnLoginEnter.visibility = View.GONE
        btnLoginRegister.isEnabled = false
    }

    override fun endLogin() {
        cpvLogin.visibility = View.GONE
        btnLoginEnter.visibility = View.VISIBLE
        btnLoginRegister.isEnabled = true
    }

    override fun performLoginTransition() {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_up, R.anim.hold_position)
        finish()
    }
}
