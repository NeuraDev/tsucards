package com.campus.android.app.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.asksira.loopingviewpager.LoopingPagerAdapter
import com.campus.android.R
import com.campus.android.helpers.WindowUtils
import java.util.*


/**
 * Created by agladkov on 20.02.18.
 */
class TutorialAdapter(private val appContext: Context, val data: ArrayList<Int>) :
        LoopingPagerAdapter<Int>(appContext, data, false) {
    private val TAG: String = TutorialAdapter::class.java.simpleName

    override fun bindView(convertView: View, listPosition: Int, viewType: Int) {
        val txtTitle = convertView.findViewById<TextView>(R.id.txtTutorialTitle)
        val txtContent = convertView.findViewById<TextView>(R.id.txtTutorialText)
        val imgPlaceholder = convertView.findViewById<ImageView>(R.id.imgTutorialIcon)

        when (listPosition) {
            0 -> {
                txtTitle.text = appContext.getString(R.string.tutorial_first)
                txtContent.text = appContext.getString(R.string.tutorial_first_content)
                imgPlaceholder.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.img_tutorial_first))
            }

            1 -> {
                txtTitle.text = appContext.getString(R.string.tutorial_second)
                txtContent.text = appContext.getString(R.string.tutorial_second_content)
                imgPlaceholder.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.img_tutorial_second))
            }

            2 -> {
                txtTitle.text = appContext.getString(R.string.tutorial_third)
                txtContent.text = appContext.getString(R.string.tutorial_third_content)
                imgPlaceholder.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.jmg_tutorial_third))
            }

            3 -> {
                txtTitle.text = appContext.getString(R.string.tutorial_forth)
                txtContent.text = appContext.getString(R.string.tutorial_forth_content)
                imgPlaceholder.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.jmg_tutorial_forth))
            }

            4 -> {
                txtTitle.text = appContext.getString(R.string.tutorial_fifth)
                txtContent.text = appContext.getString(R.string.tutorial_fifth_content)
                imgPlaceholder.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.img_tutorial_fifth))
            }
        }
    }

    @SuppressLint("InflateParams")
    override fun inflateView(viewType: Int, container: ViewGroup?, listPosition: Int): View {
        val inflater = LayoutInflater.from(appContext)
        return inflater.inflate(R.layout.tutorial_page, null)
    }
}