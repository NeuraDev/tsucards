package com.campus.android.app.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.campus.android.R
import com.campus.android.app.adapters.CardsAdapter
import com.campus.android.app.interfaces.ConfigurationListener
import com.campus.android.app.interfaces.RouterProvider
import com.campus.android.app.presenters.CardsPresenter
import com.campus.android.app.views.CardView
import com.campus.android.base.BaseAdapterCallback
import com.campus.android.base.BaseChildFragment
import com.campus.android.di.App
import com.campus.android.domain.converters.CardsConverter
import com.campus.android.domain.models.Card
import com.campus.android.domain.repositories.CardsRepository
import com.campus.android.helpers.Keys
import com.campus.android.helpers.ListConfig
import com.campus.android.helpers.ScreenKeys
import kotlinx.android.synthetic.main.fragment_cards.*
import javax.inject.Inject


/**
 * Created by Nugaev Marat 16.08.18.
 * A simple [Fragment] subclass.
 */
class CardsFragment : BaseChildFragment(), CardView {
    private val TAG = CardsFragment::class.java.simpleName

    // MARK: - Injects
    @Inject lateinit var cardsRepository: CardsRepository
    @Inject lateinit var cardsConverter: CardsConverter

    // MARK: - Presenter setup
    @InjectPresenter
    lateinit var cardsPresenter: CardsPresenter

    @ProvidePresenter
    fun provideCardsPresenter(): CardsPresenter {
        return CardsPresenter(cardsRepository = cardsRepository, cardsConverter = cardsConverter)
    }

    private val mAdapter = CardsAdapter()

    companion object {
        fun getNewInstance(): CardsFragment {
            return CardsFragment()
        }
    }

    private var selectedCard: Card? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@CardsFragment)
        super.onCreate(savedInstanceState)

        mAdapter.attachCallback(object : BaseAdapterCallback<Card> {
            override fun onItemClick(model: Card, view: View) {

            }

            override fun onLongClick(model: Card, view: View): Boolean {
                selectedCard = model
                cvDeleteCard.visibility = View.VISIBLE
                return true
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_cards, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fabCardsAdd.setOnClickListener {
            (activity as? RouterProvider)?.getRouter()?.replaceScreen(ScreenKeys.AddCard.value, (mAdapter.itemCount == 0))
        }

        btnCardsAdd.setOnClickListener {
            (activity as? RouterProvider)?.getRouter()?.replaceScreen(ScreenKeys.AddCard.value, (mAdapter.itemCount == 0))
        }

        btnDeleteCard.setOnClickListener {
            cardsPresenter.deleteCard(id = selectedCard?.id ?: -1, token = (activity as? ConfigurationListener)
                    ?.provideConfiguration()?.token.orEmpty())
            cvDeleteCard.visibility = View.GONE
        }

        btnDeleteCardCancel.setOnClickListener {
            cvDeleteCard.visibility = View.GONE
        }

        context?.let { ctx ->
            val listConfig = ListConfig.Builder(adapter = mAdapter)
                    .setHasFixedSize(isFixedSize = true)
                    .setHasNestedScroll(isNestedScroll = false)
                    .build(context = ctx)
            listConfig.applyConfig(context = ctx, recyclerView = recyclerCards)
        }
    }

    override fun onResume() {
        super.onResume()
        cardsPresenter.fetchCards(id = (activity as? ConfigurationListener)?.provideConfiguration()?.id?.toInt() ?: -1,
                token = (activity as? ConfigurationListener)?.provideConfiguration()?.token.orEmpty())
    }

    // MARK: - View implementation
    override fun startLoading() {
        try {
            recyclerCards.visibility = View.GONE
            cvCardsNoItems.visibility = View.GONE
            fabCardsAdd.visibility = View.GONE
            cpvCards.visibility = View.VISIBLE
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun endLoading() {
        try {
            cpvCards.visibility = View.GONE
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun setupData(data: List<Card>) {
        try {
            Log.e(TAG, "cards data $data")

            if (mAdapter.hasItems) {
                mAdapter.updateItems(itemsList = data)
            } else {
                mAdapter.setList(dataList = data)
            }

            if (data.isNotEmpty()) {
                context?.getSharedPreferences(getString(R.string.app_name), 0)
                        ?.edit()?.putBoolean(Keys.Cards.value, true)?.apply()
            }

            recyclerCards.visibility = View.VISIBLE
            fabCardsAdd.visibility = View.VISIBLE
            cvCardsNoItems.visibility = View.GONE
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun successDeleted() {
        cardsPresenter.fetchCards(id = (activity as? ConfigurationListener)?.provideConfiguration()?.id?.toInt() ?: -1,
                token = (activity as? ConfigurationListener)?.provideConfiguration()?.token.orEmpty())
    }

    override fun showError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun showError(message: Int) {
        Toast.makeText(context, getString(message), Toast.LENGTH_LONG).show()
    }

    override fun showNoItems() {
        try {
            cvCardsNoItems.visibility = View.VISIBLE
            recyclerCards.visibility = View.GONE
            fabCardsAdd.visibility = View.GONE
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }
}
