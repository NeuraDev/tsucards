package com.campus.android.app.adapters

import android.annotation.SuppressLint
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.campus.android.R
import com.campus.android.base.BaseAdapter
import com.campus.android.base.BaseViewHolder
import com.campus.android.domain.models.Key
import junit.runner.Version.id

/**
 * Created by Alex Gladkov on 15.08.18.
 * Created by Nugaev Marat 16.08.18.
 * Adapter for keys fragment
 */
class KeysAdapter : BaseAdapter<Key>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Key> {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_key, parent, false))
    }

    class ViewHolder(itemView: View) : BaseViewHolder<Key>(itemView = itemView) {
        private var cvBackground: LinearLayout = itemView.findViewById<View>(R.id.llKey) as LinearLayout
        private var txtTitle: TextView = itemView.findViewById<View>(R.id.txtKeyTitle) as TextView
        private var txtValue: TextView = itemView.findViewById<View>(R.id.txtKeyValue) as TextView
        private var txtStatus: TextView = itemView.findViewById<View>(R.id.txtKeyStatus) as TextView
        private var imgStatus: ImageView = itemView.findViewById<ImageView>(R.id.imageStatus) as ImageView

        @SuppressLint("SetTextI18n")
        override fun bind(model: Key) {
            when (model.status) {
                1 -> {
                    cvBackground.background = ContextCompat.getDrawable(itemView.context, R.drawable.key_waiting)
                    imgStatus.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.ic_key_waiting))
                    txtStatus.text = itemView.context.getString(R.string.key_status_waiting).toUpperCase()
                }

                3 -> {
                    cvBackground.background = ContextCompat.getDrawable(itemView.context, R.drawable.key_declined)
                    imgStatus.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.ic_key_close))
                    txtStatus.text = itemView.context.getString(R.string.key_status_declined).toUpperCase()
                }

                2 -> {
                    cvBackground.background = ContextCompat.getDrawable(itemView.context, R.drawable.key_approved)
                    imgStatus.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.ic_key_confirm))
                    txtStatus.text = itemView.context.getString(R.string.key_status_approved).toUpperCase()
                }

                0 -> {
                    cvBackground.background = ContextCompat.getDrawable(itemView.context, R.drawable.key_review)
                    imgStatus.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.ic_key_review))
                    txtStatus.text = itemView.context.getString(R.string.key_status_review).toUpperCase()
                }
            }

            txtTitle.text = model.title
            try {
                txtValue.text = "${itemView.context.getString(R.string.key_expired)} ${model.expired.toString("dd.MM.yyyy")}"
            } catch (e: Exception) {
                txtValue.visibility = View.GONE
            }
        }
}
}