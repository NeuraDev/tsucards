package com.campus.android.app.fragments.containers

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.campus.android.R
import com.campus.android.app.fragments.CardsFragment
import com.campus.android.app.fragments.ProfileFragment
import com.campus.android.app.interfaces.RouterProvider
import com.campus.android.base.BaseContainer
import com.campus.android.helpers.Keys
import com.campus.android.helpers.ScreenKeys
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Forward
import java.io.File

/**
 * Created by Alex Gladkov on 15.08.18.
 * Container for cards tab
 */
class ProfileContainer : BaseContainer() {
    private val TAG: String = ProfileContainer::class.java.simpleName

    companion object {
        fun getNewInstance(name: String): ProfileContainer {
            val fragment = ProfileContainer()
            val args = Bundle()
            args.putString(Keys.Name.value, name)

            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.container, container, false)
    }

    override fun doubleTap() {
        getCicerone().router.backTo(ScreenKeys.Profile.value)
    }

    override fun push(type: String, data: Any?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragment = childFragmentManager.findFragmentById(R.id.container)
        if (fragment == null) {
            getCicerone().router.replaceScreen(ScreenKeys.Profile.value, 0)
        }
    }

    private var navigator: Navigator? = null
    override fun getNavigator(): Navigator? {
        return if (navigator == null) {
            navigator = object: SupportAppNavigator(activity, childFragmentManager, R.id.container) {
                override fun createActivityIntent(screenKey: String?, data: Any?): Intent? {
                    return null
                }

                override fun createFragment(screenKey: String, data: Any?): Fragment? {
                    return when (screenKey) {
                        ScreenKeys.Profile.value -> ProfileFragment.getNewInstance()
                        else -> null
                    }
                }

                override fun setupFragmentTransactionAnimation(command: Command?, currentFragment: Fragment?, nextFragment: Fragment?,
                                                               fragmentTransaction: FragmentTransaction) {
                    super.setupFragmentTransactionAnimation(command, currentFragment, nextFragment, fragmentTransaction)
                    if (command is Forward && nextFragment !is ProfileFragment) {
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right,
                                R.anim.slide_in_right, R.anim.slide_out_left)
                    }
                }

                override fun exit() {
                    super.exit()
                    activity?.let { (it as RouterProvider).getRouter().exit() }
                }
            }

            navigator as SupportAppNavigator
        } else {
            navigator!!
        }
    }

    fun uploadAvatar(image: File) {
        val fragment = childFragmentManager.findFragmentById(R.id.container)
        if (fragment is ProfileFragment) {
            fragment.uploadAvatar(image = image)
        }
    }

    fun updateDate(selectedDate: String?) {
        val fragment = childFragmentManager.findFragmentById(R.id.container)
        if (fragment is ProfileFragment) {
            fragment.selectDate(selectedDate = selectedDate)
        }
    }

}