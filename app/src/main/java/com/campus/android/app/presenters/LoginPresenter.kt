package com.campus.android.app.presenters

import android.util.Log
import android.widget.Toast
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.campus.android.R
import com.campus.android.app.activities.MainActivity
import com.campus.android.app.views.LoginView
import com.campus.android.domain.repositories.AuthRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

/**
 * Created by Alex Gladkov on 18.08.18.
 * Presenter for LoginActivity
 */
@InjectViewState
class LoginPresenter(private val authRepository: AuthRepository): MvpPresenter<LoginView>() {
    private val TAG: String = LoginPresenter::class.java.simpleName

    fun login(email: String, password: String) {
        if (email.isEmpty()) {
            viewState.showError(message = R.string.error_email_empty)
            return
        }

        if (password.isEmpty()) {
            viewState.showError(message = R.string.error_password_empty)
            return
        }

        viewState.startLogin()
        authRepository.login(email = email, password = password)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    authRepository.fetchConfiguration()
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { configuration ->
                                configuration.email = email
                                configuration.token = it.token
                                configuration.id = it.id.toLong()

                                authRepository.updateConfiguration(configuration = configuration)
                                        .subscribeOn(Schedulers.computation())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe { _ ->
                                            viewState.endLogin()
                                            viewState.performLoginTransition()
                                        }
                            }
                }, { error ->
                    viewState.endLogin()
                    if (error is HttpException) {
                        val responseBody = error.response().errorBody()
                        viewState.showError(message = responseBody?.string().orEmpty())
                    } else {
                        viewState.showError(message = R.string.error_login)
                    }
                })
    }
}