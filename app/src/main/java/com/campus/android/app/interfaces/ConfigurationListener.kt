package com.campus.android.app.interfaces

import com.campus.android.domain.models.Configuration

interface ConfigurationListener {
    fun provideConfiguration(): Configuration
    fun updateSelfConfiguration()
}