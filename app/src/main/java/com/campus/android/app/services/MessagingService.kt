package com.campus.android.app.services

import android.content.Intent
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MessagingService: FirebaseMessagingService() {
    private val TAG = MessagingService::class.java.simpleName

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        remoteMessage?.let {
            Log.e(TAG, "type: ${remoteMessage.data["type"]}, data ${remoteMessage.data["data"]}")

            if (remoteMessage.data.isNotEmpty()) {
//                remoteMessage.data["type"]?.let {
//                    val intent = Intent(EnumCollections.IntentActions.Push.value)
//                            .putExtra(EnumCollections.Params.Type.value, remoteMessage.data["type"])
//                            .putExtra(EnumCollections.Params.Data.value, remoteMessage.data["data"])
//                    this.sendBroadcast(intent)
//                }
            }
        }
    }
}