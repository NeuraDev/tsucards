package com.campus.android.app.fragments.containers

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.campus.android.R
import com.campus.android.app.fragments.RegisterDataFragment
import com.campus.android.app.fragments.RegisterPasswordFragment
import com.campus.android.app.fragments.RegisterPhotoFragment
import com.campus.android.app.interfaces.RouterProvider
import com.campus.android.base.BaseContainer
import com.campus.android.helpers.Keys
import com.campus.android.helpers.ScreenKeys
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.SupportAppNavigator
import java.io.File

class RegisterContainer: BaseContainer() {
    private val TAG: String = RegisterContainer::class.java.simpleName

    companion object {
        fun getNewInstance(name: String): RegisterContainer {
            val fragment = RegisterContainer()
            val args = Bundle()
            args.putString(Keys.Name.value, name)

            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.container, container, false)
    }

    override fun doubleTap() {
        getCicerone().router.backTo(ScreenKeys.RegisterData.value)
    }

    override fun push(type: String, data: Any?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragment = childFragmentManager.findFragmentById(R.id.container)
        if (fragment == null) {
            getCicerone().router.replaceScreen(ScreenKeys.RegisterData.value, 0)
        }
    }

    private var navigator: Navigator? = null
    override fun getNavigator(): Navigator? {
        return if (navigator == null) {
            navigator = object: SupportAppNavigator(activity, childFragmentManager, R.id.container) {

                override fun createActivityIntent(screenKey: String?, data: Any?): Intent? {
                    return null
                }

                override fun createFragment(screenKey: String, data: Any?): Fragment? {
                    return when (screenKey) {
                        ScreenKeys.RegisterData.value -> RegisterDataFragment.getNewInstance()
                        ScreenKeys.ImagePreview.value -> RegisterPhotoFragment.getNewInstance()
                        ScreenKeys.RegisterPassword.value -> RegisterPasswordFragment.getNewInstance(photo = (data as? File))
                        else -> null
                    }
                }

                override fun exit() {
                    super.exit()
                    activity?.let { (it as RouterProvider).getRouter().exit() }
                }
            }

            navigator as SupportAppNavigator
        } else {
            navigator!!
        }
    }
}