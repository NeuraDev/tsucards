package com.campus.android.app.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Alex Gladkov on 15.08.18.
 * View for Main Activity
 */
@StateStrategyType(value = OneExecutionStateStrategy::class)
interface MainView: MvpView {
    fun configurationLoaded(index: Int)
}