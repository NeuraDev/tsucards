package com.campus.android.app.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.campus.android.domain.models.Key

/**
 * Created by Alex Gladkov on 15.08.18.
 * View for Key Fragment
 */
@StateStrategyType(value = OneExecutionStateStrategy::class)
interface KeyView: MvpView {
    fun startLoading()
    fun endLoading()
    fun showError(message: String)
    fun showNoItems()
    fun setupData(data: List<Key>)
}