package com.campus.android.app.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.campus.android.domain.models.Card

/**
 * Created by Nugaev Marat on 16.08.2018.
 */

@StateStrategyType(value = OneExecutionStateStrategy::class)
        interface CardView: MvpView{
    fun setupData(data: List<Card>)
    fun startLoading()
    fun endLoading()
    fun showError(message: String)
    fun showError(message: Int)
    fun successDeleted()
    fun showNoItems()
}