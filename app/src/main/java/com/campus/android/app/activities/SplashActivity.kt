package com.campus.android.app.activities

import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.campus.android.R
import com.campus.android.di.App
import com.campus.android.domain.repositories.AuthRepository
import com.campus.android.helpers.Keys
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SplashActivity: MvpAppCompatActivity() {

    // MARK: - Injects
    @Inject lateinit var authRepository: AuthRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(activity = this@SplashActivity)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        authRepository.fetchConfiguration()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { configuration ->
                    if (configuration.token == "") {
                        if (getSharedPreferences(getString(R.string.app_name), 0)
                                .getBoolean(Keys.Tutorial.value, false)) {
                            startActivity(Intent(applicationContext, LoginActivity::class.java))
                        } else {
                            startActivity(Intent(applicationContext, TutorialActivity::class.java))
                        }
                    } else {
                        startActivity(Intent(applicationContext, MainActivity::class.java))
                    }

                    finish()
                }
    }
}