package com.campus.android.app.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.campus.android.domain.models.Message

/**
 * Created by Nugaev Marat on 16.08.2018.
 * Modified by Alex Gladkov on 20.08.2018
 */

@StateStrategyType(value = OneExecutionStateStrategy::class)
interface MessageView: MvpView {
    fun showNoItems()
    fun showError(message: String)
    fun setupData(data: List<Message>)
    fun startLoading()
    fun endLoading()
}