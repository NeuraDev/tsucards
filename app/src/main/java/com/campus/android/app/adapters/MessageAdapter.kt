package com.campus.android.app.adapters

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.campus.android.R
import com.campus.android.base.BaseAdapter
import com.campus.android.base.BaseViewHolder
import com.campus.android.domain.models.Message
import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat


/**
 * Created by Alex Gladkov on 15.08.18.
 * Created by Nugaev Marat 16.08.18.
 * Adapter for keys fragment
 */
class MessageAdapter : BaseAdapter<Message>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Message> {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_message, parent, false))
    }

    class ViewHolder(itemView: View) : BaseViewHolder<Message>(itemView = itemView) {
        private val dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss")
        private val TAG = ViewHolder::class.java.simpleName
        private var txtTitle: TextView = itemView.findViewById<View>(R.id.txtMessageTitle) as TextView
        private var txtValue: TextView = itemView.findViewById<View>(R.id.txtMessageValue) as TextView
        private var txtDate: TextView = itemView.findViewById<View>(R.id.txtMessageDate) as TextView
        private var imageMessage: ImageView = itemView.findViewById<ImageView>(R.id.imageMessage) as ImageView

        @SuppressLint("SetTextI18n")
        override fun bind(model: Message) {
            txtTitle.text = model.title
            txtValue.text = model.value

            try {
                val dateTime = LocalDate.parse(model.date, dateFormatter)
                if (dateTime.plusDays(1).isAfter(LocalDate())) {
                    txtDate.text = LocalDateTime.parse(model.date, dateFormatter).toString("dd.MM.yyyy HH:mm:ss")
                } else {
                    txtDate.text = LocalDateTime.parse(model.date, dateFormatter).toString("dd.MM.yyyy")
                }
            } catch (e: Exception) {
                txtDate.visibility = View.GONE
            }

            when {
                model.type == 1 -> imageMessage.setImageResource(R.mipmap.like)
                model.type == -1 -> imageMessage.setImageResource(R.mipmap.dislike)
                model.type == 0 -> imageMessage.setImageResource(R.mipmap.select)
                else -> imageMessage.visibility = View.GONE
            }
        }
    }

}