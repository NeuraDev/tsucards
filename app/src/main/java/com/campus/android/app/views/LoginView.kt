package com.campus.android.app.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Alex Gladkov on 18.08.18.
 * View for LoginActivity
 */
@StateStrategyType(value = OneExecutionStateStrategy::class)
interface LoginView: MvpView {
    fun showError(message: Int)
    fun showError(message: String)
    fun startLogin()
    fun endLogin()
    fun performLoginTransition()
}