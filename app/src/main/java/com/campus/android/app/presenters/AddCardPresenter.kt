package com.campus.android.app.presenters

import android.util.Log
import android.widget.Toast
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.campus.android.R
import com.campus.android.app.views.AddCardView
import com.campus.android.domain.repositories.CardsRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

/**
 * Created by Alex Gladkov on 20.08.2018.
 * Presenter for AddCardActivity
 */
@InjectViewState
class AddCardPresenter(private val cardsRepository: CardsRepository): MvpPresenter<AddCardView>() {
    private val TAG = AddCardPresenter::class.java.simpleName

    fun addCard(token: String, cardNumber: String, month: String, year: String, userId: Int) {
        val realMonth = if (month.isEmpty()) 0 else month.toInt()
        val realYear = if (year.isEmpty()) 0 else year.toInt()

        if (cardNumber.length != 16) {
            viewState.showError(message = R.string.add_card_wrong_card_format)
            return
        }

        if (realMonth > 12) {
            viewState.showError(message = R.string.error_add_card_month)
            return
        }

        viewState.startSending()
        cardsRepository.addCard(token = token, cardNumber = cardNumber, month = realMonth, year = realYear,
                userId = userId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.endSending()
                    viewState.showSuccess()
                }, { error ->
                    viewState.endSending()
                    if (error is HttpException) {
                        val responseBody = error.response().errorBody()
                        viewState.showError(message = responseBody?.string().orEmpty())
                    } else {
                        viewState.showError(message = R.string.error_add_card)
                    }
                })
    }

}



