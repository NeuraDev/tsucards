package com.campus.android.app.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.campus.android.R
import com.campus.android.app.interfaces.RouterProvider
import com.campus.android.app.presenters.CardsPresenter
import com.campus.android.app.views.CardView
import com.campus.android.base.BaseChildFragment
import com.campus.android.di.App
import com.campus.android.domain.models.Card
import com.campus.android.domain.repositories.AuthRepository
import com.campus.android.domain.repositories.OtherRepository
import com.campus.android.helpers.DateHelper
import com.campus.android.helpers.Keys
import com.campus.android.helpers.ListConfig
import com.campus.android.helpers.ScreenKeys
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_cards.*
import kotlinx.android.synthetic.main.fragment_register_password.*
import org.joda.time.LocalDate
import retrofit2.HttpException
import java.io.File
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class RegisterPasswordFragment : BaseChildFragment() {
    private val TAG = RegisterPasswordFragment::class.java.simpleName

    // MARK: - Injects
    @Inject
    lateinit var authRepository: AuthRepository
    @Inject
    lateinit var otherRepository: OtherRepository

    companion object {
        fun getNewInstance(photo: File?): RegisterPasswordFragment {
            val fragment = RegisterPasswordFragment()
            val args = Bundle()
            args.putSerializable(Keys.Photo.value, photo)

            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@RegisterPasswordFragment)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_register_password, container, false)
    }

    private fun startSending() {
        btnRegisterPasswordFinish.visibility = View.GONE
        cpvRegisterPassword.visibility = View.VISIBLE
    }

    private fun endSending() {
        btnRegisterPasswordFinish.visibility = View.VISIBLE
        cpvRegisterPassword.visibility = View.GONE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnRegisterPasswordFinish.setOnClickListener {
            if (textRegisterEmail.text.toString().isEmpty()) {
                Toast.makeText(context, getString(R.string.error_email_empty), Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (textRegisterPassword.text.toString().isEmpty()) {
                Toast.makeText(context, getString(R.string.error_password_empty), Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (!textRegisterEmail.text.toString().contains("@") || !textRegisterEmail.text.toString().contains(".")) {
                Toast.makeText(context, getString(R.string.error_email_invalid), Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (textRegisterPassword.text.toString() != textRegisterConfirm.text.toString()) {
                Toast.makeText(context, getString(R.string.error_password_invalid), Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            startSending()
            authRepository.fetchConfiguration()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { configuration ->
                        val photo = arguments?.get(Keys.Photo.value) as? File
                        if (photo == null) {
                            endSending()
                            Toast.makeText(context, getString(R.string.error_register_no_photo), Toast.LENGTH_LONG).show()
                        } else {
                            otherRepository.uploadFile(file = photo)
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe({ guid ->
                                        authRepository.register(email = textRegisterEmail.text.toString(),
                                                password = textRegisterPassword.text.toString(), name = configuration.name.orEmpty(),
                                                surname = configuration.surname.orEmpty(),
                                                //     date = LocalDate().toString("yyyy-MM-dd"),
                                                date = DateHelper.convertModelToApi(model = configuration.birthDay.orEmpty()),
                                                thirdName = configuration.patronymic.orEmpty(), avatar = guid, status = configuration.status)
                                                .subscribeOn(Schedulers.newThread())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe({
                                                    configuration.token = it.token
                                                    configuration.id = it.id.toLong()
                                                    configuration.email = textRegisterEmail.text.toString()

                                                    authRepository.updateConfiguration(configuration = configuration)
                                                            .subscribeOn(Schedulers.newThread())
                                                            .observeOn(AndroidSchedulers.mainThread())
                                                            .subscribe { _ ->
                                                                endSending()
                                                                (activity as? RouterProvider)?.getRouter()?.replaceScreen(ScreenKeys.Main.value)
                                                            }
                                                }, { error ->
                                                    endSending()
                                                    if (error is HttpException) {
                                                        val responseBody = error.response().errorBody()
                                                        Toast.makeText(context, responseBody?.string().orEmpty(), Toast.LENGTH_LONG).show()
                                                    } else {
                                                        Toast.makeText(context, R.string.error_register, Toast.LENGTH_LONG).show()
                                                    }


                                                })
                                    }, { error ->
                                        endSending()
                                        if (error is HttpException) {
                                            val responseBody = error.response().errorBody()
                                            Toast.makeText(context, responseBody?.string().orEmpty(), Toast.LENGTH_LONG).show()
                                        } else {
                                            Toast.makeText(context, R.string.error_register, Toast.LENGTH_LONG).show()
                                        }


                                        (activity as? RouterProvider)?.getRouter()?.replaceScreen(ScreenKeys.Image.value)
                                    })
                        }
                    }
        }
    }
}
