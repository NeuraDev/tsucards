package com.campus.android.app.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.campus.android.R
import com.campus.android.app.activities.MainActivity
import com.campus.android.base.BaseAdapter
import com.campus.android.base.BaseViewHolder
import com.campus.android.domain.models.Card
import com.campus.android.domain.models.Gate
import kotlinx.android.synthetic.main.cell_card.view.*

/**
 * Created by Nugaev Marat on 16.08.2018.
 * Adapter for cards fragment
 */
class GatesAdapter : BaseAdapter<Gate>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Gate> {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_gate, parent, false))
    }

    class ViewHolder(itemView: View) : BaseViewHolder<Gate>(itemView = itemView) {
        private val TAG: String = ViewHolder::class.java.simpleName
        private var txtTitle: TextView = itemView.findViewById<View>(R.id.txtGateTitle) as TextView

        @SuppressLint("SetTextI18n")
        override fun bind(model: Gate) {
            txtTitle.text = "${model.title}, ${model.place}"
        }
    }
}