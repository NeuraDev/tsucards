package com.campus.android.app.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.campus.android.R
import com.campus.android.helpers.Keys
import com.prolificinteractive.materialcalendarview.CalendarDay
import kotlinx.android.synthetic.main.activity_calendar.*
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
import java.util.*

class CalendarActivity: AppCompatActivity() {
    private val TAG = CalendarActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)
        val currentDate = intent?.extras?.getString(Keys.Calendar.value)

        currentDate?.let {
            try {
                val dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd")
                val date = LocalDate.parse(it, dateFormat)
                mcvCalendar.currentDate = CalendarDay.from(date.yearOfEra, date.monthOfYear,
                        date.dayOfMonth)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btnCalendarSave.setOnClickListener {
            val data = Intent()
            val calendar = Calendar.getInstance()
            calendar.set(mcvCalendar.currentDate.year, mcvCalendar.currentDate.month, mcvCalendar.currentDate.day)
            calendar.set(Calendar.DAY_OF_MONTH, mcvCalendar.currentDate.day)

            data.putExtra(Keys.Calendar.value, LocalDate.fromCalendarFields(calendar).toString("yyyy-MM-dd"))
            setResult(MvpAppCompatActivity.RESULT_OK, data)
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.hold_position, R.anim.slide_down)
    }
}