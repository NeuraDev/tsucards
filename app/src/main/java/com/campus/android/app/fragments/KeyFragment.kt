package com.campus.android.app.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.campus.android.R
import com.campus.android.app.adapters.KeysAdapter
import com.campus.android.app.interfaces.ConfigurationListener
import com.campus.android.app.interfaces.RouterProvider
import com.campus.android.app.presenters.KeysPresenter
import com.campus.android.app.views.KeyView
import com.campus.android.base.BaseChildFragment

import com.campus.android.di.App
import com.campus.android.domain.models.Key
import com.campus.android.domain.repositories.KeysRepository
import com.campus.android.domain.repositories.ReaderRepository
import com.campus.android.helpers.ListConfig
import com.campus.android.helpers.ScreenKeys
import kotlinx.android.synthetic.main.fragment_cards.*
import kotlinx.android.synthetic.main.fragment_keys.*
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 * Created by Nugaev Marat 16.08.18.
 */
class KeyFragment : BaseChildFragment(), KeyView {

    // MARK: - Injects
    @Inject lateinit var keysRepository: KeysRepository
    @Inject lateinit var readerRepository: ReaderRepository

    // MARK: - Presenter setup
    @InjectPresenter
    lateinit var keysPresenter: KeysPresenter

    @ProvidePresenter
    fun provideKeysPresenter(): KeysPresenter {
        return KeysPresenter(keysRepository = keysRepository, readersRepository = readerRepository)
    }

    // MARK: - UI Setup
    private val mAdapter = KeysAdapter()

    companion object {
        fun getNewInstance(): KeyFragment {
            return KeyFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@KeyFragment)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_keys, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fabKeysAdd.setOnClickListener {
            (activity as? RouterProvider)?.getRouter()?.replaceScreen(ScreenKeys.AddKey.value)
        }

        btnKeysAdd.setOnClickListener {
            (activity as? RouterProvider)?.getRouter()?.replaceScreen(ScreenKeys.AddKey.value)
        }

        tbKeys.setOnClickListener {
            (parentFragment as? RouterProvider)?.getRouter()?.navigateTo(ScreenKeys.Profile.value)
        }

        context?.let { ctx ->
            val listConfig = ListConfig.Builder(adapter = mAdapter)
                    .setHasFixedSize(isFixedSize = true)
                    .setHasNestedScroll(isNestedScroll = false)
                    .build(context = ctx)
            listConfig.applyConfig(context = ctx, recyclerView = recyclerKeys)
        }
    }

    override fun onResume() {
        super.onResume()
        keysPresenter.fetchKeys(userId = (activity as? ConfigurationListener)?.provideConfiguration()?.id?.toInt() ?: -1,
                token = (activity as? ConfigurationListener)?.provideConfiguration()?.token.orEmpty())
    }

    // MARK: - View implementation
    override fun setupData(data: List<Key>) {
        try {
            if (mAdapter.hasItems) {
                mAdapter.updateItems(itemsList = data)
            } else {
                mAdapter.setList(dataList = data)
            }

            recyclerKeys.visibility = View.VISIBLE
            fabKeysAdd.visibility = View.VISIBLE
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun startLoading() {
        try {
            recyclerKeys.visibility = View.GONE
            cvKeysNoItems.visibility = View.GONE
            fabKeysAdd.visibility = View.GONE
            cpvKeys.visibility = View.VISIBLE
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun endLoading() {
        try {
            cpvKeys.visibility = View.GONE
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun showError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun showNoItems() {
        cvKeysNoItems.visibility = View.VISIBLE
    }
}
