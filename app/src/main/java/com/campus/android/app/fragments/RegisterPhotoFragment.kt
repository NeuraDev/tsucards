package com.campus.android.app.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.campus.android.R
import com.campus.android.app.adapters.CardsAdapter
import com.campus.android.app.interfaces.RouterProvider
import com.campus.android.app.presenters.CardsPresenter
import com.campus.android.app.views.CardView
import com.campus.android.base.BaseChildFragment
import com.campus.android.data.implementations.AuthRepositoryImpl
import com.campus.android.di.App
import com.campus.android.domain.models.Card
import com.campus.android.domain.repositories.AuthRepository
import com.campus.android.helpers.ListConfig
import com.campus.android.helpers.ScreenKeys
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_cards.*
import kotlinx.android.synthetic.main.fragment_register_data.*
import kotlinx.android.synthetic.main.fragment_register_photo.*
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class RegisterPhotoFragment: BaseChildFragment() {

    // MARK: - Injects
    @Inject lateinit var authRepository: AuthRepository

    companion object {
        fun getNewInstance(): RegisterPhotoFragment {
            return RegisterPhotoFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@RegisterPhotoFragment)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_register_photo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnRegisterCamera.setOnClickListener {
            (activity as? RouterProvider)?.getRouter()?.replaceScreen(ScreenKeys.Image.value)
        }
    }
}
