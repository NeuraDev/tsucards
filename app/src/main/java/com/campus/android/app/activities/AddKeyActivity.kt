package com.campus.android.app.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import com.campus.android.R
import kotlinx.android.synthetic.main.add_key.*
import android.R.attr.data
import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import com.campus.android.di.App
import com.campus.android.domain.models.Card
import com.campus.android.domain.models.Gate
import com.campus.android.domain.repositories.AuthRepository
import com.campus.android.domain.repositories.CardsRepository
import com.campus.android.domain.repositories.KeysRepository
import com.campus.android.helpers.ImageLoader
import com.campus.android.helpers.Keys
import com.campus.android.helpers.WindowUtils
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import okhttp3.ResponseBody
import org.json.JSONObject

/**
 * Created by Nugaev Marat on 14.08.2018.
 */

class AddKeyActivity : AppCompatActivity() {
    private val TAG = AddKeyActivity::class.java.simpleName

    // MARK: - Injects
    @Inject lateinit var authRepository: AuthRepository
    @Inject lateinit var cardsRepository: CardsRepository
    @Inject lateinit var keysRepository: KeysRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(activity = this@AddKeyActivity)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_key)

        flAddKeyObject.setOnClickListener {
            startActivityForResult(Intent(applicationContext, SelectGateActivity::class.java), 100)
            overridePendingTransition(R.anim.slide_in_up, R.anim.hold_position)
        }

        val cardsList = ArrayList<Card>()
        var selectedCard: Card? = null

        btnAddCardKey.setOnClickListener {
            onBackPressed()
        }

        authRepository.fetchConfiguration()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ configuration ->
                    cardsRepository.fetchCardsFromDB()
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ cards ->
                                cardsList.addAll(cards)
                                val parsedCards = cards.map { it.title }.toMutableList()
                                parsedCards.add(0, getString(R.string.add_key_select_card))
                                spinnerAddKeyCards.adapter = ArrayAdapter<String>(this,
                                        android.R.layout.simple_list_item_1, parsedCards)
                                spinnerAddKeyCards.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                        if (cardsList.isNotEmpty()) {
                                            try {
                                                selectedCard = cardsList[position - 1]
                                            } catch (e: ArrayIndexOutOfBoundsException) {
                                                Log.e(TAG, "error index out")
                                            }
                                        }
                                    }
                                }
                            })
                })


        btnAddKeySend.setOnClickListener {
            when {
                selectedGate == null -> Toast.makeText(applicationContext, getString(R.string.add_key_select_object), Toast.LENGTH_LONG).show()
                selectedCard == null -> Toast.makeText(applicationContext, getString(R.string.add_key_select_card), Toast.LENGTH_LONG).show()
                else -> {
                    textAddKeyComment.isEnabled = false
                    btnAddKeySend.visibility = View.GONE
                    authRepository.fetchConfiguration()
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ configuration ->
                                keysRepository.addKey(token = configuration.token, cardId = selectedCard?.id ?: -1,
                                        userId = configuration.id, gateId = selectedGate?.id ?: -1,
                                        comment = textAddKeyComment.text.toString())
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe({
                                            Toast.makeText(applicationContext, getString(R.string.add_key_success), Toast.LENGTH_LONG).show()
                                            onBackPressed()
                                        }, {
                                            //                                      if (it is HttpException) {
    //                                          val responseBody = it.response().errorBody()
    //                                          Toast.makeText(applicationContext, getString(R.string.error_add_key) + " " +
    //                                                  responseBody?.string().orEmpty(), Toast.LENGTH_LONG).show()
    //                                      } else {
    //                                          Toast.makeText(applicationContext, getString(R.string.error_add_key), Toast.LENGTH_LONG).show()
    //                                      }

                                            Toast.makeText(applicationContext, getString(R.string.error_add_key), Toast.LENGTH_LONG).show()
                                            btnAddKeySend.visibility = View.VISIBLE
                                            textAddKeyComment.isEnabled = true
                                        })
                            })
                }
            }

        }
    }

    private var selectedGate: Gate? = null
    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                selectedGate = data?.extras?.get(Keys.Gate.value) as? Gate
                if (selectedGate?.avatar.orEmpty().isEmpty()) {
                    imgAddKeyAvatar.visibility = View.GONE
                } else {
                    ImageLoader().loadImageCenterInside(url = selectedGate?.avatar.orEmpty(),
                            imageView = imgAddKeyAvatar, width = WindowUtils.getScreenWidth(this@AddKeyActivity),
                            height = WindowUtils.convertDpToPixel(200f, applicationContext))
                    imgAddKeyAvatar.visibility = View.VISIBLE
                }

                txtAddKeyObject.text = "${selectedGate?.title.orEmpty()}, ${selectedGate?.place.orEmpty()}"
            }
        }
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.hold_position, R.anim.slide_down)
    }
}
