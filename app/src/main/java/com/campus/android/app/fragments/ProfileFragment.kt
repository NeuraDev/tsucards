package com.campus.android.app.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.campus.android.R
import com.campus.android.app.interfaces.ConfigurationListener
import com.campus.android.app.interfaces.RouterProvider
import com.campus.android.base.BaseChildFragment
import com.campus.android.data.models.ApiProfile
import com.campus.android.di.App
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.File
import javax.inject.Inject
import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.content.Context.INPUT_METHOD_SERVICE
import android.graphics.Bitmap
import com.campus.android.app.activities.MainActivity
import com.campus.android.data.models.ApiProfileSend
import com.campus.android.data.models.ApiProfileWithoutSend
import com.campus.android.domain.repositories.*
import com.campus.android.helpers.*
import com.google.android.gms.flags.impl.SharedPreferencesFactory.getSharedPreferences
import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
import retrofit2.HttpException
import java.util.*
import java.io.ByteArrayOutputStream
import java.io.FileOutputStream


/**
 * A simple [Fragment] subclass.
 * Created by Nugaev Marat 16.08.18.
 */
class ProfileFragment : BaseChildFragment() {
    private val TAG = ProfileFragment::class.java.simpleName
    private var isEditing = false
    private lateinit var spinnerData: ArrayList<String>

    companion object {
        fun getNewInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }

    // MARK: - @Injects
    @Inject
    lateinit var authRepository: AuthRepository
    @Inject
    lateinit var cardsRepository: CardsRepository
    @Inject
    lateinit var keysRepository: KeysRepository
    @Inject
    lateinit var messagesRepository: MessagesRepository
    @Inject
    lateinit var otherRepository: OtherRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@ProfileFragment)
        super.onCreate(savedInstanceState)
        spinnerData = arrayListOf(getString(R.string.register_status_student),
                getString(R.string.register_status_worker), getString(R.string.register_status_guest))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnProfileExit.setOnClickListener {
            authRepository.fetchConfiguration()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { configuration ->
                        configuration.token = ""

                        cardsRepository.deleteAll()
                        keysRepository.deleteAll()
                        messagesRepository.deleteAll()

                        val sharedPreferences = context?.getSharedPreferences(getString(R.string.app_name), 0)
                        sharedPreferences?.edit()?.putBoolean(Keys.Cards.value, false)?.apply()

                        authRepository.updateConfiguration(configuration = configuration)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({ _ ->
                                    (activity as? RouterProvider)?.getRouter()?.replaceScreen(ScreenKeys.Login.value)
                                })
                    }
        }

        btnProfileSave.setOnClickListener {
            isEditing = !isEditing
            if (!isEditing) {
                if (textProfileInfoDate.text.toString().length < 10) {
                    Toast.makeText(context, getString(R.string.error_date), Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }

                llProfileInfo.visibility = View.VISIBLE
                llProfileInfoEdit.visibility = View.GONE
                context?.let {
                    btnProfileSave.setImageDrawable(ContextCompat.getDrawable(it, R.mipmap.pencil))
                    val imm = it.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view.windowToken, 0)
                }

                saveChanges()
            } else {
                llProfileInfo.visibility = View.GONE
                llProfileInfoEdit.visibility = View.VISIBLE
                context?.let {
                    btnProfileSave.setImageDrawable(ContextCompat.getDrawable(it, R.drawable.ic_check))
                    textProfileInfoSurname.postDelayed({
                        val keyboard = it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        keyboard.showSoftInput(textProfileInfoSurname, 0)
                    }, 200)
                }
            }
        }

        spProfile.setItems(spinnerData)


        imgProfileAvatar.setOnClickListener {
            (activity as? RouterProvider)?.getRouter()?.replaceScreen(ScreenKeys.Image.value)
        }

        authRepository.fetchConfiguration()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { configuration ->
                    try {
                        txtProfileInfoSurname.text = configuration.surname
                        txtProfileInfoName.text = configuration.name
                        txtProfileInfoThirdName.text = configuration.patronymic
                        txtProfileInfoDate.text = DateHelper.convertApiToModel(backend = configuration.birthDay.orEmpty())
                        txtProfileInfoStatus.text = configuration.status

                        textProfileInfoSurname.setText(configuration.surname)
                        textProfileInfoName.setText(configuration.name)
                        textProfileInfoThirdName.setText(configuration.patronymic)
                        textProfileInfoDate.setText(DateHelper.convertApiToModel(backend = configuration.birthDay.orEmpty()))


                        context?.let { ctx ->
                            if (configuration.avatar.orEmpty().isEmpty()) {
                                if (isAdded) {
                                    imgProfileAvatar.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.img_profile_avatar))
                                    imgProfileBackdrop.visibility = View.GONE
                                }
                            } else {
                                if (isAdded) {
                                    imgProfileBackdrop.visibility = View.VISIBLE
                                    ImageLoader().loadImageCenterInside(url = configuration.avatar.orEmpty(),
                                            imageView = imgProfileBackdrop,
                                            width = WindowUtils.getScreenWidth(activity = activity),
                                            height = WindowUtils.convertDpToPixel(260f, ctx))
                                    ImageLoader().loadImage(url = configuration.avatar.orEmpty(), imageView = imgProfileAvatar)
                                }
                            }
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    authRepository.fetchProfile(id = configuration.id.toInt(), token = configuration.token)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ apiProfile ->
                                try {
                                    txtProfileInfoSurname.text = apiProfile.surname
                                    txtProfileInfoName.text = apiProfile.name
                                    txtProfileInfoThirdName.text = apiProfile.patronymic
                                    txtProfileInfoDate.text = DateHelper.convertApiToModel(backend = apiProfile.birthday)
                                    txtProfileInfoStatus.text = apiProfile.status

                                    textProfileInfoSurname.setText(apiProfile.surname)
                                    textProfileInfoName.setText(apiProfile.name)
                                    textProfileInfoThirdName.setText(apiProfile.patronymic)
                                    textProfileInfoDate.setText(DateHelper.convertApiToModel(backend = apiProfile.birthday))

                                    spProfile.selectedIndex = spinnerData.indexOf(apiProfile.status)

                                    context?.let { ctx ->
                                        if (apiProfile.foto.isEmpty()) {
                                            if (isAdded) {
                                                imgProfileAvatar.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.img_profile_avatar))
                                                imgProfileBackdrop.visibility = View.GONE
                                            }
                                        } else {
                                            if (isAdded) {
                                                imgProfileBackdrop.visibility = View.VISIBLE
                                                ImageLoader().loadImageCenterInside(url = apiProfile.foto,
                                                        imageView = imgProfileAvatar,
                                                        width = WindowUtils.getScreenWidth(activity = activity),
                                                        height = WindowUtils.convertDpToPixel(260f, ctx))
                                                ImageLoader().loadImage(url = apiProfile.foto, imageView = imgProfileAvatar)
                                            }
                                        }
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }

                                configuration.surname = apiProfile.surname
                                configuration.name = apiProfile.name
                                configuration.patronymic = apiProfile.patronymic
                                configuration.avatar = apiProfile.foto
                                configuration.birthDay = apiProfile.birthday
                                configuration.status = apiProfile.status

                                authRepository.updateConfiguration(configuration = configuration)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe { _ ->
                                            Log.e(TAG, "profile updated")
                                        }
                            }, {

                            })
                }
    }

    private fun saveChanges() {
        authRepository.fetchConfiguration()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { configuration ->
                    authRepository.updateProfile(apiProfile = ApiProfileWithoutSend(id = configuration.id.toFloat(),
                            surname = textProfileInfoSurname.text.toString(), name = textProfileInfoName.text.toString(),
                            patronymic = textProfileInfoThirdName.text.toString(),
                            birthday = DateHelper.convertModelToApi(model = textProfileInfoDate.text.toString()),
                            status = spinnerData[spProfile.selectedIndex]),
                            id = configuration.id.toInt(), token = configuration.token)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ _ ->
                                txtProfileInfoSurname.text = textProfileInfoSurname.text.toString()
                                txtProfileInfoName.text = textProfileInfoName.text.toString()
                                txtProfileInfoThirdName.text = textProfileInfoThirdName.text.toString()
                                txtProfileInfoDate.text = textProfileInfoDate.text.toString()
                                txtProfileInfoStatus.text = spinnerData[spProfile.selectedIndex]

                                configuration.surname = textProfileInfoSurname.text.toString()
                                configuration.name = textProfileInfoName.text.toString()
                                configuration.patronymic = textProfileInfoThirdName.text.toString()
                                configuration.birthDay = textProfileInfoDate.text.toString()
                                configuration.status = spinnerData[spProfile.selectedIndex]

                                authRepository.updateConfiguration(configuration = configuration)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe { _ ->
                                            Log.e(TAG, "profile updated")
                                        }
                            }, {
                                Toast.makeText(context, getString(R.string.error_profile_update), Toast.LENGTH_LONG).show()
                            })
                }
    }

    fun selectDate(selectedDate: String?) {
        if (selectedDate.orEmpty().isNotEmpty()) {
            txtProfileInfoDate.text = selectedDate
            textProfileInfoDate.setText(selectedDate)
        }
    }

    fun uploadAvatar(image: File) {
        authRepository.fetchConfiguration()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { configuration ->
                    otherRepository.uploadFile(file = image)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ photoId ->
                                authRepository.updateProfile(apiProfile = ApiProfileSend(id = configuration.id.toFloat(),
                                        surname = textProfileInfoSurname.text.toString(), name = textProfileInfoName.text.toString(),
                                        patronymic = textProfileInfoThirdName.text.toString(),
                                        birthday = DateHelper.convertModelToApi(model = textProfileInfoDate.text.toString()),
                                        fotoId = photoId, status = spinnerData[spProfile.selectedIndex]),
                                        id = configuration.id.toInt(), token = configuration.token)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe({ _ ->
                                            authRepository.fetchProfile(id = (activity as? ConfigurationListener)?.provideConfiguration()?.id?.toInt()
                                                    ?: -1,
                                                    token = (activity as? ConfigurationListener)?.provideConfiguration()?.token.orEmpty())
                                                    .subscribeOn(Schedulers.newThread())
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe { apiProfile ->
                                                        txtProfileInfoStatus.text = spinnerData
                                                                .firstOrNull { it == apiProfile.status }.orEmpty()

                                                        context?.let { ctx ->
                                                            if (apiProfile.foto.isEmpty()) {
                                                                imgProfileAvatar.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.img_profile_avatar))
                                                                imgProfileBackdrop.visibility = View.GONE
                                                            } else {
                                                                imgProfileBackdrop.visibility = View.VISIBLE
                                                                ImageLoader().loadImageCenterInside(url = apiProfile.foto,
                                                                        imageView = imgProfileBackdrop,
                                                                        width = WindowUtils.getScreenWidth(activity = activity),
                                                                        height = WindowUtils.convertDpToPixel(260f, ctx))
                                                                ImageLoader().loadImage(url = apiProfile.foto, imageView = imgProfileAvatar)
                                                            }
                                                        }
                                                    }
                                        }, { error ->
                                            if (error is HttpException) {
                                                val responseBody = error.response().errorBody()
                                                Toast.makeText(context, responseBody?.string().orEmpty(), Toast.LENGTH_LONG).show()
                                            } else {
                                                Toast.makeText(context, getString(R.string.error_update_profile), Toast.LENGTH_LONG).show()
                                            }
                                        })
                            }, { error ->
                                if (error is HttpException) {
                                    val responseBody = error.response().errorBody()
                                    Toast.makeText(context, responseBody?.string().orEmpty(), Toast.LENGTH_LONG).show()
                                } else {
                                    Toast.makeText(context, getString(R.string.error_update_photo), Toast.LENGTH_LONG).show()
                                }
                            })
                }
    }
}
