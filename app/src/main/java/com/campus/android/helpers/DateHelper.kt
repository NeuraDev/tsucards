package com.campus.android.helpers

import android.util.Log
import com.campus.android.app.fragments.ProfileFragment
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat

/**
 * Created by neura on 24.08.18.
 */
class DateHelper {
    private val TAG = DateHelper::class.java.simpleName
    companion object {
        
        private val dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss")

        fun convertApiToModel(backend: String) : String {
            return try {
                LocalDate.parse(backend, dateFormat).toString("dd-MM-yyyy")
            } catch (e: Exception) {
                backend
            }
        }

        fun convertModelToApi(model: String): String {
            val newDateFormat = DateTimeFormat.forPattern("dd-MM-yyyy")
            return try {
                LocalDate.parse(model, newDateFormat).toString("yyyy-MM-dd")
            } catch (e: Exception) {
                ""
            }
        }
    }
}