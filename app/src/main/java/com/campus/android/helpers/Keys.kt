package com.campus.android.helpers

/**
 * Created by agladkov on 09.01.18.
 */
enum class Keys(val value: String) {
    Back("BackKey"), Configuration("ConfigurationKey"), Register("RegisterKey"), Bio("BioKey"),
    Campaign("CampaignKey"), Url("UrlKey"), Badge("BadgeKey"), Photo("PhotoKey"), Empty("EmptyKey"),
    Gate("GateKey"), Tutorial("TutorialKey"), Calendar("CalendarKey"), Cards("CardsKey"),

    // Pin keys
    PinSource("PinKey"), PinRegister("RegisterKey"), PinAuth("AuthKey"),

    // Name for keys store
    Name("KeyName"),

    // Title for navigation
    Title("TitleName"),

    // Title for document
    Document("DocumentName"), Content("ContentKey"),

    // User
    CurrentUser("UserName"), Surname("SurnameKey"),

    // Order
    Price("PriceKey"), PriceText("PriceTextKey"), Order("OrderKey"),

    // Main
    Device("DeviceKey"), Problem("ProblemKey"), Token("TokenKey"),

    // Help
    LeftDevice("LeftDeviceKey"), RightDevice("RightDeviceKey"), CenterDevice("CenterDeviceKey")
}
