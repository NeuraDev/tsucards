package com.campus.android.helpers;

import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Field;

/**
 * Created by agladkov on 14.03.18.
 */

public class BottomNavigationViewHelper {

//    public static void applyBottomNavFont(BottomNavigationView view) {
//        // The BottomNavigationView widget doesn't provide a native way to set the appearance of
//        // the text views. So we have to hack in to the view hierarchy here.
//        for (int i = 0; i < view.getChildCount(); i++) {
//            View child = view.getChildAt(i);
//            if (child instanceof BottomNavigationMenuView) {
//                BottomNavigationMenuView menu = (BottomNavigationMenuView) child;
//                for (int j = 0; j < menu.getChildCount(); j++) {
//                    View item = menu.getChildAt(j);
//                    View smallItemText = item.findViewById(android.support.design.R.id.smallLabel);
//                    if (smallItemText instanceof TextView) {
//                        ((TextView) smallItemText).setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
//                        ((TextView) smallItemText).setTypeface(ResourcesCompat.getFont(view.getContext(),
//                                R.font.pfbeausanspro_regular));
//                    }
//                    View largeItemText = item.findViewById(android.support.design.R.id.largeLabel);
//                    if (largeItemText instanceof TextView) {
//                        ((TextView) largeItemText).setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
//                        ((TextView) largeItemText).setTypeface(ResourcesCompat.getFont(view.getContext(),
//                                R.font.pfbeausanspro_regular));
//                    }
//                }
//            }
//        }
//    }

    public static void removeShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShiftingMode(false);
                item.setPadding(0, 20, 0, 0);
                // set once again checked value, so view will be updated
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }



//    public static void addBadge(@NotNull String badgeValue, @NotNull int position, @Nullable BottomNavigationView navigation) {
//        if (navigation == null) return;
//        BottomNavigationMenuView bottomMenu = (BottomNavigationMenuView) navigation.getChildAt(0);
//
//        if (bottomMenu == null || bottomMenu.getChildCount() <= position) return;
//        BottomNavigationItemView bottomView = (BottomNavigationItemView) bottomMenu.getChildAt(position);
//
//        View badge = LayoutInflater.from(navigation.getContext()).inflate(R.layout.badge_bar, bottomMenu, false);
//        badge.setTag(Keys.Badge.getValue());
//        TextView textView = badge.findViewById(R.id.txtBadgeValue);
//        textView.setText(badgeValue);
//
//        FrameLayout.LayoutParams badgeLayout = new FrameLayout.LayoutParams(badge.getLayoutParams());
//        badgeLayout.gravity = Gravity.END;
//        badgeLayout.rightMargin = (int) WindowUtils.Companion.convertDpToPixel(8f, navigation.getContext());
//        badgeLayout.topMargin = (int) WindowUtils.Companion.convertDpToPixel(2f, navigation.getContext());
//
//        bottomView.addView(badge, badgeLayout);
//    }
//
//    public static void clearBadge(int position, BottomNavigationView navigation) {
//        if (navigation == null) return;
//        BottomNavigationMenuView bottomMenu = (BottomNavigationMenuView) navigation.getChildAt(0);
//
//        if (bottomMenu == null || bottomMenu.getChildCount() <= position) return;
//        BottomNavigationItemView bottomView = (BottomNavigationItemView) bottomMenu.getChildAt(position);
//        recursiveLoopChildren(bottomView);
//    }

    private static void recursiveLoopChildren(ViewGroup parent) {
        for (int i = 0; i < parent.getChildCount(); i++) {
            final View child = parent.getChildAt(i);
            if (child instanceof ViewGroup) {
                if (child.getTag() == Keys.Badge.getValue()) {
                    parent.removeView(child);
                } else {
                    recursiveLoopChildren((ViewGroup) child);
                }
            } else {
                if (child != null && child.getTag() == Keys.Badge.getValue()) {
                    parent.removeView(child);
                }
            }
        }
    }
}
