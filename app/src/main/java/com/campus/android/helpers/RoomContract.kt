package com.campus.android.helpers

class RoomContract {
    companion object {

        const val DATABASE_APP = "campus.db"

        const val TABLE_SERVICES = "services"
        const val TABLE_CONFIGRATIONS = "configuration"
        const val TABLE_CARDS = "cards"
        const val TABLE_KEYS = "keys"
        const val TABLE_GATES = "gates"
        const val TABLE_MESSAGES = "messages"

        private const val SELECT_COUNT = "SELECT COUNT(*) FROM "
        private const val SELECT_FROM = "SELECT * FROM "

        const val SELECT_SERVICES_COUNT = SELECT_COUNT + TABLE_SERVICES
        const val SELECT_SERVICE = SELECT_FROM + TABLE_SERVICES
    }

}