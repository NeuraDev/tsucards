package com.campus.android.helpers

/**
 * Created by Alex Gladkov on 28.12.17.
 * Contract for remote operations
 */
class RemoteContract {

    companion object {
        val baseUrl = "http://univercity.su/api/"
        val baseUrlDev = "http://coribri.com/api/"
        val imageBaseUrl = "http://campus.colibri.com/api"

        const val EMAIL = "email"
        const val PASSWORD = "password"
        const val TYPE = "type"

        const val SUCCESS = "success"
        const val DATA = "data"
        const val ERROR = "error"

        const val ERROR_CODE = "code"
        const val ERROR_DESCRIPTION = "description"

        const val ID = "id"
        const val TOKEN = "token"
        const val EXPIRES = "expires_in"
        const val FCM_TOKEN = "fcmToken"
        const val VENDORS = "vendors"

        const val MESSAGE = "message"
        const val MESSAGE_ID = "messageId"

        const val RATING = "rating"
        const val COMMENT = "comment"
        const val REASONS = "reasons"

        const val PHONE = "phone"
        const val LOGIN = "login"
        const val NAME = "name"
        const val CAMPAIGN = "campaign"
        const val SURNAME = "surname"

        const val MONTH = "month"
        const val YEAR = "year"

        const val DEVICE = "device"
        const val PROBLEM = "problem"
        const val DESCRIPTION = "description"
        const val LICENSE = "licenseId"
        const val FILE = "file"

        const val CODE = "code"

        const val SERVER_ERROR = "Ошибка загрузки данных"

        const val TYPE_ANDROID = "AndroidClient"
    }
}
