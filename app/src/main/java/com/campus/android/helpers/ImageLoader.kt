package com.campus.android.helpers

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.text.TextUtils
import android.util.Log
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.io.File

/**
 * Created by agladkov on 25.12.17.
 * Class helper for loading images
 */
class ImageLoader {
    private val TAG: String = ImageLoader::class.java.simpleName

    fun loadImage(url: Int, imageView: ImageView?) {
        if (url == 0) {
            printError(reason = "res is empty")
            return
        }

        if (imageView == null) {
            printError(reason = "imageView is null")
            return
        }

        Picasso.with(imageView.context)
                .load(url)
                .into(imageView)
    }

    fun loadImage(url: File, imageView: ImageView?) {
        if (imageView == null) {
            printError(reason = "imageView is null")
            return
        }

        Picasso.with(imageView.context)
                .load(url)
                .into(object: Target{
                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

                    }

                    override fun onBitmapFailed(errorDrawable: Drawable?) {
//                        DrawableCompat.setTint(imageView.drawable, ContextCompat.getColor(imageView.context,
//                                R.color.sddSecondaryTintColor))
                    }

                    override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom?) {
                        imageView.setImageBitmap(bitmap)
                    }
                })
    }

    fun loadImage(url: String, imageView: ImageView?) {
        if (TextUtils.isEmpty(url)) {
            printError(reason = "url is empty")
            return
        }

        if (imageView == null) {
            printError(reason = "imageView is null")
            return
        }

        Picasso.with(imageView.context)
                .load(url)
                .into(imageView)
    }

    fun loadImageCenterInside(url: String, imageView: ImageView?, width: Float, height: Float) {
        if (TextUtils.isEmpty(url)) {
            printError(reason = "url is empty")
            return
        }

        if (imageView == null) {
            printError(reason = "imageView is null")
            return
        }

        Picasso.with(imageView.context)
                .load(url)
                .resize(width.toInt(), height.toInt())
                .centerCrop()
                .into(imageView)
    }

    private fun printError(reason: String) {
        Log.e(TAG, "Can't load image. REASON: $reason")
    }
}
