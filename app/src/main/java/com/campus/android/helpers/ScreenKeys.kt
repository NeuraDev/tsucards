package com.campus.android.helpers

/**
 * Created by agladkov on 22.12.17.
 * Screen keys enum
 */
enum class ScreenKeys(val value: String) {
    Url("UrlKey"), Keys("KeysKey"), Cards("CardsKey"), Messages("MessagesKey"), AddKey("AddKeyScreen"),
    AddCard("AddCardScreen"), Profile("ProfileKey"), Login("LoginKey"),
    RegisterData("RegisterDataScreen"), RegisterPassword("RegisterPasswordScreen"), Main("MainScreen"),
    Image("ImageScreen"), Calendar("CalendarScreen"), ImagePreview("ImagePreviewScreen")
}