package com.campus.android.base

import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.WindowManager
import com.arellomobile.mvp.MvpAppCompatActivity
import android.support.annotation.ColorInt
import android.content.res.Resources.Theme
import android.util.TypedValue
import com.campus.android.helpers.WindowConfig

/**
 * Created by agladkov on 14.02.18.
 */
abstract class BaseActivity: MvpAppCompatActivity() {
    companion object {
//       val appTheme = EnumCollections.Theme.SDD
    }

    override fun onCreate(savedInstanceState: Bundle?) {
//        setTheme(appTheme.styleRes)
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            WindowConfig.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
            window.statusBarColor = ContextCompat.getColor(applicationContext, android.R.color.transparent)
        }
    }
}