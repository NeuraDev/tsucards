package com.campus.android.base

import android.support.v7.widget.RecyclerView

/**
 * Created by agladkov on 25.12.17.
 */
abstract class BaseAdapter<P>: RecyclerView.Adapter<BaseViewHolder<P>>() {
    protected var mDataList: List<P> = ArrayList<P>()
    private var mCallback: BaseAdapterCallback<P>? = null
    var hasItems = false

    fun attachCallback(callback: BaseAdapterCallback<P>) {
        this.mCallback = callback
    }

    fun detachCallback() {
        this.mCallback = null
    }

    fun setList(dataList: List<P>) {
        (mDataList as ArrayList).addAll(dataList)
        hasItems = true
        notifyDataSetChanged()
    }

    fun addItem(newItem: P) {
        (mDataList as ArrayList).add(newItem)
        notifyItemInserted(mDataList.size - 1)
    }

    fun addItemToTop(newItem: P) {
        (mDataList as ArrayList).add(0, newItem)
        notifyItemInserted(0)
    }

    fun updateItems(itemsList: List<P>) {
        (mDataList as ArrayList).clear()
        setList(itemsList)
    }

    fun clearAllItems() {
        (mDataList as ArrayList).clear()
        notifyDataSetChanged()
    }

    fun getItems(): List<P> {
        return mDataList
    }

    override fun onBindViewHolder(holder: BaseViewHolder<P>, position: Int) {
        holder.bind(mDataList[position])

        holder.itemView.setOnClickListener {
            mCallback?.onItemClick(mDataList[position], holder.itemView)
        }
        holder.itemView.setOnLongClickListener {
            if (mCallback == null) {
                false
            } else {
                mCallback!!.onLongClick(mDataList[position], holder.itemView)
            }

        }
    }

    override fun getItemCount(): Int {
        return mDataList.count()
    }
}