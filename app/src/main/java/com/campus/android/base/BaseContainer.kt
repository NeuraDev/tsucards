package com.campus.android.base

import android.os.Bundle
import android.util.Log
import com.arellomobile.mvp.MvpAppCompatFragment
import com.campus.android.app.interfaces.BackButtonListener
import com.campus.android.app.interfaces.RouterProvider
import com.campus.android.R
import com.campus.android.di.App
import com.campus.android.helpers.Keys
import com.campus.android.helpers.LocalCiceroneHolder
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by agladkov on 11.01.18.
 * Use this for tabs fragments (ex. ServiceContainer, ProfileContainer, etc)
 */
abstract class BaseContainer: MvpAppCompatFragment(), RouterProvider, BackButtonListener {
    private val TAG: String = BaseContainer::class.java.simpleName
    private val containerName = arguments?.getString(Keys.Name.value)

    @Inject lateinit var ciceroneHolder: LocalCiceroneHolder

    abstract fun doubleTap()
    abstract fun push(type: String, data: Any?)

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this)
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        getCicerone().navigatorHolder.setNavigator(getNavigator())
    }

    override fun onPause() {
        getCicerone().navigatorHolder.removeNavigator()
        super.onPause()
    }

    protected fun getCicerone(): Cicerone<Router> {
        return ciceroneHolder.getCicerone(containerName.toString())
    }

    open fun getNavigator(): Navigator? {
        throw NotImplementedError()
    }

    // MARK: - BackButtonListener implementation
    override fun onBackPressed(): Boolean {
        val fragment = childFragmentManager.findFragmentById(R.id.container)
        return if (fragment != null && fragment is BackButtonListener && fragment.onBackPressed()) {
            true
        } else {
            activity?.let { (it as RouterProvider).getRouter().exit() }
            true
        }
    }

    // MARK: - RouterProvider
    override fun getRouter(): Router {
        return getCicerone().router
    }
}
