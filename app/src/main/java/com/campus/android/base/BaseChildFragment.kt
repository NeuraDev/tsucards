package com.campus.android.base

import android.content.Context
import android.os.Bundle
import android.view.animation.Animation
import com.arellomobile.mvp.MvpAppCompatFragment
import com.campus.android.app.interfaces.BackButtonListener
import com.campus.android.app.interfaces.RouterProvider

/**
 * Created by agladkov on 11.01.18.
 * Use this for any child fragment in container (ex. ServiceFragment, ProfileFragment)
 */
open class BaseChildFragment: MvpAppCompatFragment(), BackButtonListener {
    var isAnimationDisabled: Boolean = false
    var isDisabledRecommended: Boolean = false
    private val TAG: String = BaseChildFragment::class.java.simpleName
    private var isFirstCreating: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isFirstCreating = false
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        if (isAnimationDisabled) {
            val a = object: Animation() {}
            a.duration = 0
            return a
        }

        if (isDisabledRecommended) {
            isAnimationDisabled = true
            isDisabledRecommended = false
        }
        return super.onCreateAnimation(transit, enter, nextAnim)
    }

    override fun onResume() {
        isAnimationDisabled = false
        super.onResume()
    }

    override fun onPause() {
        isDisabledRecommended = true
        super.onPause()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    // MARK: - BackButtonListener implementation
    override fun onBackPressed(): Boolean {
        (parentFragment as RouterProvider).getRouter().exit()
        return true
    }
}
