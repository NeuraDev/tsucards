package com.campus.android.data.implementations

import com.campus.android.data.room.RoomAppDataSource
import com.campus.android.data.services.RemoteMessageService
import com.campus.android.domain.converters.MessagesConverter
import com.campus.android.domain.models.Message
import com.campus.android.domain.repositories.MessagesRepository
import io.reactivex.Single
import kotlin.concurrent.thread

/**
 * Created by Alex Gladkov on 18.08.18.
 * Implementation for MessagesRepository
 */
class MessagesRepositoryImpl(private val remoteMessageService: RemoteMessageService,
                             private val roomAppDataSource: RoomAppDataSource,
                             private val messagesConverter: MessagesConverter): MessagesRepository {

    override fun deleteAll() {
        thread {
            roomAppDataSource.messagesDao().deleteAll()
        }
    }

    override fun fetchMessages(): Single<List<Message>> {
        return Single.create { subscriber ->
            try {
                subscriber.onSuccess(roomAppDataSource.messagesDao().getMessages()
                        .map { messagesConverter.dbToModel(messageEntity = it) })
            } catch (e: Exception) {
                subscriber.onError(e)
            }
        }
    }

    override fun fetchRemoteMessages(id: Int, token: String): Single<List<Message>> {
        return remoteMessageService.fetchMessages(authHeader = token, id = id)
                .flatMap { response ->
                    Single.just(response.map { messagesConverter.apiToModel(apiMessage = it) }) }
    }

    override fun updateStorage(newItems: List<Message>): Single<Boolean> {
        return Single.create { subscriber ->
            try {
                roomAppDataSource.messagesDao().deleteAll()

                newItems.forEach({ newItem ->
                    roomAppDataSource.messagesDao().updateOrCreate(messagesConverter.modelToDB(message = newItem))
                })

                subscriber.onSuccess(true)
            } catch (e: Exception) {
                subscriber.onError(e)
            }
        }
    }

}