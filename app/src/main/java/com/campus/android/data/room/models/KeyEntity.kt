package com.campus.android.data.room.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.campus.android.helpers.RoomContract
import org.joda.time.LocalDate

/**
 * Create by Alex Gladkov on 21.08.2018
 * Data class for key model (data layer)
 */
@Entity(tableName = RoomContract.TABLE_KEYS)
data class KeyEntity(@PrimaryKey val id: Int, val status: Int, val gateId: Int, val cardId: Int,
                     val userId: Int, val expired: String)