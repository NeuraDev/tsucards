package com.campus.android.data.services

import com.campus.android.data.models.ApiCard
import com.campus.android.data.models.ApiGate
import io.reactivex.Single
import retrofit2.http.*

/**
 * Created by Alex Gladkov on 18.08.18.
 * Remote service for Cards
 */
interface RemoteReadersService {

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @GET("./gates")
    fun fetchReaders(@Header("Authorization") authHeader: String): Single<List<ApiGate>>
}