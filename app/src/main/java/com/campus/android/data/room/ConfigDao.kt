package com.campus.android.data.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.campus.android.data.room.models.ConfigEntity
import com.campus.android.helpers.RoomContract

/**
 * Created by agladkov on 12.02.18.
 */
@Dao
interface ConfigDao {

    // Get app configuration (can be expanded in future)
    @Query("SELECT * FROM " + RoomContract.TABLE_CONFIGRATIONS + " LIMIT 1")
    fun getConfigurations(): List<ConfigEntity>

    // Add or update configuration in DB
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateOrCreate(configEntity: ConfigEntity)

    @Query("DELETE FROM " + RoomContract.TABLE_CONFIGRATIONS)
    fun deleteAll()
}