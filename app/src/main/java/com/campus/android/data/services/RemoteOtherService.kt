package com.campus.android.data.services

import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

/**
 * Created by Alex Gladkov on 20.08.18.
 * Remote service for other operations
 */
interface RemoteOtherService {

    @POST("./files")
    fun uploadFile(@Body file: RequestBody): Single<String>
}