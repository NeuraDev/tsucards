package com.campus.android.data.room.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.campus.android.helpers.RoomContract


/**
 * Created by Alex Gladkov on 20.08.18.
 */
@Entity(tableName = RoomContract.TABLE_CARDS)
data class CardEntity(@PrimaryKey val id: Int, val cardNumber: String, val month: Int, val year: Int,
                      val userId: Int)