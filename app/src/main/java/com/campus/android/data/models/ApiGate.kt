package com.campus.android.data.models

data class ApiGate(val id: Float, val name: String, val place: String, val twoFactors: Boolean,
                   val foto: String)