package com.campus.android.data.models

data class ApiFcm(val type: String = "client", val platform: String = "android", val token: String)