package com.campus.android.data.implementations

import android.util.Log
import com.campus.android.data.models.*
import com.google.gson.JsonObject
import com.campus.android.data.room.RoomAppDataSource
import com.campus.android.data.services.RemoteAuthService
import com.campus.android.domain.converters.ConfigModelConverter
import com.campus.android.domain.models.Configuration
import com.campus.android.domain.repositories.AuthRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody

/**
 * Created by Alex Gladkov on 18.08.18.
 * Implementation for AuthRepository
 */
class AuthRepositoryImpl(val remoteAuthService: RemoteAuthService,
                         private val roomAppDataSource: RoomAppDataSource,
                         private val configurationConverter: ConfigModelConverter): AuthRepository {
    private val TAG = AuthRepositoryImpl::class.java.simpleName

    override fun updateProfile(apiProfile: ApiProfileSend, token: String, id: Int): Single<Any> {
        return remoteAuthService.updateProfile(authHeader = token, id = id, data = apiProfile)
    }

    override fun updateProfile(apiProfile: ApiProfileWithoutSend, token: String, id: Int): Single<Any> {
        return remoteAuthService.updateProfileWithoutPhoto(authHeader = token, id = id,
                data = apiProfile)
    }

    override fun sendFcm(fcmToken: String, token: String): Single<Boolean> {
        return remoteAuthService.sendFcm(data = ApiFcm(token = fcmToken), authHeader = token)
                .flatMap { Single.just(true) }
    }

    override fun fetchConfiguration(): Single<Configuration> {
        return Single.create { subscriber ->
            try {
                val configurations = roomAppDataSource.configDao().getConfigurations()

                if (configurations.isNotEmpty()) {
                    subscriber.onSuccess(configurationConverter.dbToModel(configEntity = configurations[0]))
                } else {
                    val defaultInstance = Configuration.defaultInstance()
                    roomAppDataSource.configDao().updateOrCreate(configEntity = configurationConverter
                            .modelToDb(configurationModel = defaultInstance))
                    subscriber.onSuccess(defaultInstance)
                }
            } catch (e: Exception) {
                val defaultInstance = Configuration.defaultInstance()
                roomAppDataSource.configDao().updateOrCreate(configEntity = configurationConverter
                        .modelToDb(configurationModel = defaultInstance))
                subscriber.onSuccess(defaultInstance)
            }
        }
    }

    override fun updateConfiguration(configuration: Configuration): Single<Boolean> {
        return Single.create { subscriber ->
            roomAppDataSource.configDao().updateOrCreate(configurationConverter.modelToDb(configurationModel = configuration))
            subscriber.onSuccess(true)
        }
    }

    override fun fetchProfile(token: String, id: Int): Single<ApiProfile> {
        return remoteAuthService.fetchProfile(authHeader = token, id = id)
    }

    override fun login(email: String, password: String): Single<ApiLoginResponse> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", email)
        jsonObject.addProperty("password", password)

        return remoteAuthService.performAuth(data = ApiLogin(username = email, password = password))
    }

    override fun register(email: String, password: String, name: String, surname: String, thirdName: String,
                          date: String, avatar: String, status: String): Single<ApiLoginResponse> {
        return remoteAuthService.registerUser(data = ApiRegister(email = email, password = password, name = name,
                surname = surname, patronymic = thirdName, fotoId = avatar, birthday = date, status = status))
    }
}