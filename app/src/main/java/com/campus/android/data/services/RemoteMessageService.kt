package com.campus.android.data.services

import com.campus.android.data.models.ApiMessage
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Path

/**
 * Created by Alex Gladkov on 18.08.18.
 * Remote service for Messages
 */
interface RemoteMessageService {

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @GET("./users/{user_id}/notifications")
    fun fetchMessages(@Header("Authorization") authHeader: String,
                  @Path(value = "user_id", encoded = true) id: Int): Single<List<ApiMessage>>
}