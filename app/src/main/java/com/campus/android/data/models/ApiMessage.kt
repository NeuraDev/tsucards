package com.campus.android.data.models

/**
 * Created by Alex Gladkov on 20.08.18.
 * Data class for message (remote layer)
 */
data class ApiMessage(val id: String, val date: String, val title: String, val text: String,
                      val type: Float, val typeName: String, val userId: Float)