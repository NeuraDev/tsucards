package com.campus.android.data.models

data class ApiLogin(val username: String, val password: String)
data class ApiLoginResponse(val token: String, val id: Int)