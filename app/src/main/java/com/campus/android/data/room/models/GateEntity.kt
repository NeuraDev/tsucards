package com.campus.android.data.room.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.campus.android.helpers.RoomContract

@Entity(tableName = RoomContract.TABLE_GATES)
data class GateEntity(@PrimaryKey val id: Int, val title: String, val place: String, val isTwoFactors: Boolean)