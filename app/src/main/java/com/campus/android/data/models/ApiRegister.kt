package com.campus.android.data.models

/**
 * Created by Alex Gladkov on 20.08.18.
 * File for remote register operations
 */
data class ApiRegister(val email: String, val password: String, val name: String, val surname: String,
                       val patronymic: String, val birthday: String, val fotoId: String, val status: String)