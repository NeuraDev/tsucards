package com.campus.android.data.models

data class ApiCard(val id: Int = 0, val pan: String, val year: Int, val month: Int, val userId: Int,
                   val bankName: String?)