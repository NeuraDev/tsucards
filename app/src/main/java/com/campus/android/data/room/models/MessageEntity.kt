package com.campus.android.data.room.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.campus.android.helpers.RoomContract

/**
 * Created by Alex Gladkov on 20.08.18.
 * Data message model for entity layer
 * */
@Entity(tableName = RoomContract.TABLE_MESSAGES)
data class MessageEntity(@PrimaryKey val id: String, val title: String, val value: String, val date: String,
                         val avatar: String, val type: Int)