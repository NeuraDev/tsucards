package com.campus.android.data.room.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.campus.android.helpers.RoomContract

/**
 * Created by agladkov on 12.02.18.
 */
@Entity(tableName = RoomContract.TABLE_CONFIGRATIONS)
data class ConfigEntity(@PrimaryKey val id: Long, val token: String, val avatar: String?,
                        val name: String?, val surname: String?, val patronymic: String?,
                        val birthDay: String?, val fcmToken: String?, val email: String?,
                        var userId: Long = 0, var firstUse: Boolean = true, val status: String)