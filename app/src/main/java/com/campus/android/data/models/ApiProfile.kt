package com.campus.android.data.models

data class ApiProfile(val id: Float, val surname: String, val name: String, val patronymic: String,
                      val birthday: String, val foto: String, val status: String)
data class ApiProfileSend(val id: Float, val surname: String, val name: String, val patronymic: String,
                          val birthday: String, val fotoId: String, val status: String)
data class ApiProfileWithoutSend(val id: Float, val surname: String, val name: String, val patronymic: String,
                          val birthday: String, val status: String)