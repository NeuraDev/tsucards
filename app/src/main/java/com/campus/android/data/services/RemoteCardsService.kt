package com.campus.android.data.services

import com.campus.android.data.models.ApiCard
import io.reactivex.Single
import retrofit2.http.*

/**
 * Created by Alex Gladkov on 18.08.18.
 * Remote service for Cards
 */
interface RemoteCardsService {

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @GET("./users/{user_id}/cards")
    fun fetchCards(@Header("Authorization") authHeader: String,
                   @Path(value = "user_id", encoded = true) id: Int): Single<List<ApiCard>>

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @POST("./cards")
    fun addCard(@Body data: ApiCard, @Header("Authorization") authHeader: String): Single<Any>

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @DELETE("./cards/{card_id}")
    fun deleteCard(@Header("Authorization") authHeader: String,
                   @Path(value = "card_id", encoded = true) id: Int): Single<Any>
}