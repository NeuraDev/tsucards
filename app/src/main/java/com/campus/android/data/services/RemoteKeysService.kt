package com.campus.android.data.services

import com.campus.android.data.models.ApiCard
import com.campus.android.data.models.ApiKey
import com.campus.android.data.models.ApiKeySend
import io.reactivex.Single
import retrofit2.http.*

/**
 * Created by Alex Gladkov on 18.08.18.
 * Remote service for Keys
 */
interface RemoteKeysService {

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @GET("./users/{user_id}/keys")
    fun fetchKeys(@Header("Authorization") authHeader: String,
                   @Path(value = "user_id", encoded = true) id: Int): Single<List<ApiKey>>

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @POST("./keys")
    fun addKey(@Body data: ApiKeySend, @Header("Authorization") authHeader: String): Single<Any>
}