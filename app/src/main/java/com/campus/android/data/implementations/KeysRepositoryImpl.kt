package com.campus.android.data.implementations

import com.campus.android.data.models.ApiKeySend
import com.campus.android.data.room.RoomAppDataSource
import com.campus.android.data.services.RemoteKeysService
import com.campus.android.domain.converters.CardsConverter
import com.campus.android.domain.converters.KeysConverter
import com.campus.android.domain.models.Key
import com.campus.android.domain.models.Message
import com.campus.android.domain.repositories.KeysRepository
import io.reactivex.Single
import kotlin.concurrent.thread

/**
 * Created by Alex Gladkov on 18.08.18.
 * Implementation for KeysRepository
 */
class KeysRepositoryImpl(private val remoteKeysService: RemoteKeysService,
                         private val keysConverter: KeysConverter,
                         private val roomAppDataSource: RoomAppDataSource): KeysRepository {

    override fun deleteAll() {
        thread {
            roomAppDataSource.keysDao().deleteAll()
        }
    }

    override fun addKey(token: String, cardId: Int, userId: Long, gateId: Int, comment: String): Single<Boolean> {
        return remoteKeysService.addKey(authHeader = token, data = ApiKeySend(gateId = gateId,
                cardId = cardId, userId = userId.toInt(), comment = comment)).flatMap { Single.just(true) }
    }

    override fun fetchKeys(token: String, userId: Int): Single<List<Key>> {
        return remoteKeysService.fetchKeys(authHeader = token, id = userId).flatMap {
            response -> Single.just(response.map { keysConverter.apiToModel(apiKey = it) }) }
    }

    override fun updateStorage(newItems: List<Key>): Single<Boolean> {
        return Single.create { subscriber ->
            try {
                roomAppDataSource.keysDao().deleteAll()

                newItems.forEach({ newItem ->
                    roomAppDataSource.keysDao().updateOrCreate(keysConverter.modelToDB(key = newItem))
                })

                subscriber.onSuccess(true)
            } catch (e: Exception) {
                subscriber.onError(e)
            }
        }
    }
}