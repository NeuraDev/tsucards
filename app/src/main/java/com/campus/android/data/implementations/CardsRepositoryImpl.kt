package com.campus.android.data.implementations

import com.google.android.gms.common.api.Api
import com.campus.android.data.models.ApiCard
import com.campus.android.data.room.RoomAppDataSource
import com.campus.android.data.services.RemoteCardsService
import com.campus.android.domain.converters.CardsConverter
import com.campus.android.domain.models.Card
import com.campus.android.domain.repositories.CardsRepository
import io.reactivex.Single
import kotlin.concurrent.thread

/**
 * Created by Alex Gladkov on 18.08.18.
 * Implementation for CardsRepository
 */
class CardsRepositoryImpl(private val remoteCardsService: RemoteCardsService,
                          private val cardsConverter: CardsConverter,
                          private val roomAppDataSource: RoomAppDataSource): CardsRepository {

    override fun deleteCard(token: String, id: Int): Single<Any> {
        return remoteCardsService.deleteCard(authHeader = token, id = id)
    }

    override fun deleteAll() {
        thread {
            roomAppDataSource.cardsDao().deleteAll()
        }
    }

    override fun updateStorage(newItems: List<ApiCard>): Single<Boolean> {
        return Single.create { subscriber ->
            try {
                roomAppDataSource.cardsDao().deleteAll()

                newItems.forEach {
                    roomAppDataSource.cardsDao().updateOrCreate(cardsConverter.apiToDB(apiCard = it))
                }

                subscriber.onSuccess(true)
            } catch (e: Exception) {
                subscriber.onSuccess(false)
            }
        }
    }

    override fun fetchCardsFromDB(): Single<List<Card>> {
        return Single.create {  subscriber ->
            try {
                subscriber.onSuccess(roomAppDataSource.cardsDao().getCards()
                        .map { cardsConverter.dbToModel(cardEntity = it) })
            } catch (e: Exception) {
                subscriber.onError(e)
            }
        }
    }

    override fun addCard(token: String, cardNumber: String, month: Int, year: Int, userId: Int): Single<Any> {
        return remoteCardsService.addCard(data = ApiCard(pan = cardNumber, year = year,
                month = month, userId = userId, bankName = null), authHeader = token)
    }

    override fun fetchCards(id: Int, token: String): Single<List<ApiCard>> {
        return remoteCardsService.fetchCards(authHeader = token, id = id)
    }
}