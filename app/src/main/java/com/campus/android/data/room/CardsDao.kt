package com.campus.android.data.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.campus.android.data.room.models.CardEntity
import com.campus.android.helpers.RoomContract

/**
 * Created by agladkov on 20.08.18.
 */
@Dao
interface CardsDao {

    // Get app configuration (can be expanded in future)
    @Query("SELECT * FROM " + RoomContract.TABLE_CARDS)
    fun getCards(): List<CardEntity>

    // Add or update configuration in DB
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateOrCreate(cardEntity: CardEntity)

    @Query("DELETE FROM " + RoomContract.TABLE_CARDS)
    fun deleteAll()
}