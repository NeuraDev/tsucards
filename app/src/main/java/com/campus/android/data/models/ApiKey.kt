package com.campus.android.data.models

/**
 * Created by Alex Gladkov on 21.08.2018
 */
data class ApiKey(val id: Float, val status: Float, val gateId: Float, val cardid: Float, val userId: Float,
                  val expired: String)
data class ApiKeySend(val id: Int = 0, val status: Int = 0, val gateId: Int, val cardId: Int,
                      val userId: Int, val comment: String)