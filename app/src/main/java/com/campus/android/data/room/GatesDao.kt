package com.campus.android.data.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.campus.android.data.room.models.CardEntity
import com.campus.android.data.room.models.GateEntity
import com.campus.android.data.room.models.KeyEntity
import com.campus.android.helpers.RoomContract

/**
 * Created by Alex Gladkov on 21.08.2018
 * Dao for operations with keys
 */
@Dao
interface GatesDao {

    // Get app configuration (can be expanded in future)
    @Query("SELECT * FROM " + RoomContract.TABLE_GATES)
    fun getGates(): List<GateEntity>

    // Add or update configuration in DB
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateOrCreate(gateEntity: GateEntity)

    @Query("DELETE FROM " + RoomContract.TABLE_GATES)
    fun deleteAll()

}