package com.campus.android.data.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.campus.android.data.room.models.*
import com.campus.android.helpers.RoomContract


/**
 * Created by Alex Gladkov on 26.12.17.
 * App database class
 */
@Database(entities = arrayOf(ConfigEntity::class, CardEntity::class,
        MessageEntity::class, KeyEntity::class, GateEntity::class), version = 10, exportSchema = true)
abstract class RoomAppDataSource: RoomDatabase() {
    abstract fun configDao(): ConfigDao
    abstract fun cardsDao(): CardsDao
    abstract fun messagesDao(): MessagesDao
    abstract fun keysDao(): KeysDao
    abstract fun gatesDao(): GatesDao

    companion object {
        private val TAG = RoomAppDataSource::class.java.simpleName

        fun buildDataSource(context: Context): RoomAppDataSource = Room.databaseBuilder(
                context.applicationContext, RoomAppDataSource::class.java, RoomContract.DATABASE_APP)
                .fallbackToDestructiveMigration()
                .build()
    }

}