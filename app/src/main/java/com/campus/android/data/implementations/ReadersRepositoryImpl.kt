package com.campus.android.data.implementations

import android.util.Log
import com.campus.android.data.room.RoomAppDataSource
import com.campus.android.data.services.RemoteReadersService
import com.campus.android.domain.converters.GatesConverter
import com.campus.android.domain.models.Gate
import com.campus.android.domain.repositories.ReaderRepository
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Alex Gladkov 21.08.2018
 * Implementation for ReaderRepository
 */
class ReadersRepositoryImpl(private val remoteReadersService: RemoteReadersService,
                            private val roomAppDataSource: RoomAppDataSource,
                            private val gatesConverter: GatesConverter): ReaderRepository {
    private val TAG = ReadersRepositoryImpl::class.java.simpleName

    override fun fetchReaders(id: Int, token: String): Single<List<Gate>> {
        return remoteReadersService.fetchReaders(authHeader = token)
                .flatMap { response -> Single.just(response.map { gatesConverter.apiToModel(apiGate = it) }) }
    }
}