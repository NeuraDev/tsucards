package com.campus.android.data.implementations

import android.util.Log
import com.campus.android.data.services.RemoteOtherService
import com.campus.android.domain.repositories.OtherRepository
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File
import okhttp3.MultipartBody



/**
 * Created by Alex Gladkov on 20.08.18.
 * Implementation for other operations
 */
class OtherRepositoryImpl(private val remoteOtherService: RemoteOtherService): OtherRepository {
    private val TAG = OtherRepositoryImpl::class.java.simpleName

    override fun uploadFile(file: File): Single<String> {
        return Single.create { subscriber ->
            val requestFile = RequestBody.create(MediaType.parse("image/jpeg"), file)
            val requestBody = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("", file.name, requestFile)
                    .build()

            remoteOtherService.uploadFile(file = requestBody)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ response ->
                        subscriber.onSuccess(response)
                    }, { error ->
                        subscriber.onError(error)
                    })
        }
    }
}