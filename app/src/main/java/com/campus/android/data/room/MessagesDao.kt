package com.campus.android.data.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.campus.android.data.room.models.MessageEntity
import com.campus.android.helpers.RoomContract

/**
 * Created by Alex Gladkov on 20.08.18.
 * Dao for Messages
 */
@Dao
interface MessagesDao {

    // Get app configuration (can be expanded in future)
    @Query("SELECT * FROM " + RoomContract.TABLE_MESSAGES)
    fun getMessages(): List<MessageEntity>

    // Add or update configuration in DB
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateOrCreate(messageEntity: MessageEntity)

    @Query("DELETE FROM " + RoomContract.TABLE_MESSAGES)
    fun deleteAll()
}