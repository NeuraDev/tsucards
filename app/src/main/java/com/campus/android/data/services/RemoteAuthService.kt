package com.campus.android.data.services

import com.campus.android.data.models.*
import com.google.gson.JsonObject
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.*

/**
 * Created by Alex Gladkov on 18.08.18.
 * Remote Service for Auth operations
 */
interface RemoteAuthService {

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @POST("./account/token/")
    fun performAuth(@Body data: ApiLogin): Single<ApiLoginResponse>

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @GET("./users/{user_id}")
    fun fetchProfile(@Header("Authorization") authHeader: String,
                     @Path(value = "user_id", encoded = true) id: Int): Single<ApiProfile>

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @PUT("./users/{user_id}/edit")
    fun updateProfile(@Header("Authorization") authHeader: String,
                      @Path(value = "user_id", encoded = true) id: Int,
                      @Body data: ApiProfileSend): Single<Any>

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @PUT("./users/{user_id}/edit")
    fun updateProfileWithoutPhoto(@Header("Authorization") authHeader: String,
                      @Path(value = "user_id", encoded = true) id: Int,
                      @Body data: ApiProfileWithoutSend): Single<Any>

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @POST("./account/register")
    fun registerUser(@Body data: ApiRegister): Single<ApiLoginResponse>

    @Headers("Content-Type: application/json", "Accept: application/json",
            "User-Agent: MobileApp/Android")
    @POST("./account/device")
    fun sendFcm(@Body data: ApiFcm, @Header("Authorization") authHeader: String): Single<Any>
}