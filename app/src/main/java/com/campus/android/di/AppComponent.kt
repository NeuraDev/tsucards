package com.campus.android.di

import com.campus.android.app.AddCardActivity
import com.campus.android.app.activities.*
import com.campus.android.app.fragments.*
import com.campus.android.base.BaseContainer
import dagger.Component
import com.campus.android.app.services.FirebaseIdService
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class, NavigationModule::class, RemoteModule::class,
        LocalNavigationModule::class, RepositoryModule::class, ConvertersModule::class,
        RoomModule::class))
@Singleton interface AppComponent {

    // Presenters

    // Activities
    fun inject(activity: LoginActivity)
    fun inject(activity: MainActivity)
    fun inject(activity: RegisterActivity)
    fun inject(activity: AddCardActivity)
    fun inject(activity: SplashActivity)
    fun inject(activity: SelectGateActivity)
    fun inject(activity: AddKeyActivity)

    // Fragments
    fun inject(fragment: BaseContainer)
    fun inject(fragment: ProfileFragment)
    fun inject(fragment: KeyFragment)
    fun inject(fragment: CardsFragment)
    fun inject(fragment: MessageFragment)
    fun inject(fragment: RegisterDataFragment)
    fun inject(fragment: RegisterPhotoFragment)
    fun inject(fragment: RegisterPasswordFragment)

    // Services
    fun inject(service: FirebaseIdService)
}


