package com.campus.android.di

import com.campus.android.domain.converters.*
import dagger.Module
import dagger.Provides

@Module
class ConvertersModule {

    @Provides
    fun provideCardsConverter(): CardsConverter {
        return CardsConverter()
    }

    @Provides
    fun provideConfigurationConverter(): ConfigModelConverter {
        return ConfigModelConverter()
    }

    @Provides
    fun provideMessagesConverter(): MessagesConverter {
        return MessagesConverter()
    }

    @Provides
    fun provideKeysConverter(): KeysConverter {
        return KeysConverter()
    }

    @Provides
    fun provideGatesConverter(): GatesConverter {
        return GatesConverter()
    }
}

