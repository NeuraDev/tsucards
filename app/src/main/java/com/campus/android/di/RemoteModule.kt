package com.campus.android.di

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.campus.android.data.services.*
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import com.campus.android.helpers.RemoteContract
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by agladkov on 28.12.17.
 */
@Module
class RemoteModule {

    @Provides
    @Singleton
    fun provideGson(): Gson =
            GsonBuilder().create()

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .addInterceptor { chain ->
                        val request = chain.request()
                        var response = chain.proceed(request)

                        var tryCount = 0
                        while (!response.isSuccessful && tryCount < 10) {

                            Log.d("intercept", "Request is not successful - $tryCount")

                            tryCount++
                            // retry the request
                            response = chain.proceed(request)
                        }

                        // otherwise just pass the original response on
                        response
                    }
                    .connectTimeout(40, TimeUnit.SECONDS)
                    .readTimeout(40, TimeUnit.SECONDS)
                    .build()

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit =
            Retrofit.Builder()
                    .baseUrl(RemoteContract.baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build()

    @Provides
    @Singleton
    fun provideRemoteAuthService(retrofit: Retrofit): RemoteAuthService {
        return retrofit.create(RemoteAuthService::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteCardsService(retrofit: Retrofit): RemoteCardsService {
        return retrofit.create(RemoteCardsService::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteKeysService(retrofit: Retrofit): RemoteKeysService {
        return retrofit.create(RemoteKeysService::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteMessageService(retrofit: Retrofit): RemoteMessageService {
        return retrofit.create(RemoteMessageService::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteOtherService(retrofit: Retrofit): RemoteOtherService {
        return retrofit.create(RemoteOtherService::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteReadersService(retrofit: Retrofit): RemoteReadersService {
        return retrofit.create(RemoteReadersService::class.java)
    }
}