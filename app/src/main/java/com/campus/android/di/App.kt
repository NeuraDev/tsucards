/**
 * Created by agladkov on 25.12.17.
 * Extension for Application and init all libs
 */
package com.campus.android.di

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.google.firebase.FirebaseApp


class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this@App)
        // Инициализация AppMetrica SDK
//        val configBuilder = YandexMetricaConfig.newConfigBuilder(getString(R.string.yandexMetricaApi))
//        YandexMetrica.activate(applicationContext, configBuilder.build())

        // Отслеживание активности пользователей
//        YandexMetrica.enableActivityAutoTracking(this)

        initializeDagger()
    }

    fun initializeDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(app = this@App))
                .roomModule(RoomModule())
                .remoteModule(RemoteModule())
                .build()
    }
}

