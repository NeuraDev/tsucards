package com.campus.android.di

import com.campus.android.data.implementations.*
import com.campus.android.data.room.RoomAppDataSource
import com.campus.android.data.services.*
import com.campus.android.domain.converters.*
import com.campus.android.domain.repositories.*
import dagger.Module
import dagger.Provides

/**
 * Created by Alex Gladkov on 18.08.18.
 * Module for repositories
 */
@Module
class RepositoryModule {

    @Provides
    fun provideAuthRepository(remoteAuthService: RemoteAuthService,
                              roomAppDataSource: RoomAppDataSource,
                              configModelConverter: ConfigModelConverter): AuthRepository {
        return AuthRepositoryImpl(remoteAuthService = remoteAuthService, configurationConverter = configModelConverter,
                roomAppDataSource = roomAppDataSource)
    }

    @Provides
    fun provideCardsRepository(remoteCardsService: RemoteCardsService,
                               roomAppDataSource: RoomAppDataSource,
                               cardsConverter: CardsConverter): CardsRepository {
        return CardsRepositoryImpl(remoteCardsService = remoteCardsService, cardsConverter = cardsConverter,
                roomAppDataSource = roomAppDataSource)
    }

    @Provides
    fun provideKeysRepository(remoteKeysService: RemoteKeysService, roomAppDataSource: RoomAppDataSource,
                              keysConverter: KeysConverter): KeysRepository {
        return KeysRepositoryImpl(remoteKeysService = remoteKeysService, roomAppDataSource = roomAppDataSource,
                keysConverter = keysConverter)
    }

    @Provides
    fun provideMessagesRepository(remoteMessageService: RemoteMessageService, roomAppDataSource: RoomAppDataSource,
                                  messagesConverter: MessagesConverter): MessagesRepository {
        return MessagesRepositoryImpl(remoteMessageService = remoteMessageService, roomAppDataSource = roomAppDataSource,
                messagesConverter = messagesConverter)
    }

    @Provides
    fun provideOtherRepository(remoteOtherService: RemoteOtherService): OtherRepository {
        return OtherRepositoryImpl(remoteOtherService = remoteOtherService)
    }

    @Provides
    fun provideReadersRepository(remoteReadersService: RemoteReadersService, roomAppDataSource: RoomAppDataSource,
                                 gatesConverter: GatesConverter): ReaderRepository {
        return ReadersRepositoryImpl(remoteReadersService = remoteReadersService, roomAppDataSource = roomAppDataSource,
                gatesConverter = gatesConverter)
    }
}