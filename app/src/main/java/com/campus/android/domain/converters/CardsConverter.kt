package com.campus.android.domain.converters

import com.campus.android.data.models.ApiCard
import com.campus.android.data.room.models.CardEntity
import com.campus.android.domain.models.Card

class CardsConverter {

    fun apiToModel(apiCard: ApiCard): Card {
        return Card(id = apiCard.id, title = apiCard.pan, month = apiCard.month,
                year = apiCard.year, type = "", bankName = apiCard.bankName.orEmpty())
    }

    fun apiToDB(apiCard: ApiCard): CardEntity {
        return CardEntity(id = apiCard.id, cardNumber = apiCard.pan, month = apiCard.month, year = apiCard.year,
                userId = apiCard.userId)
    }

    fun dbToModel(cardEntity: CardEntity): Card {
        return Card(id = cardEntity.id, title = cardEntity.cardNumber, month = cardEntity.month,
                year = cardEntity.year, type = "", bankName = "")
    }
}