package com.campus.android.domain.repositories

import com.campus.android.data.models.ApiCard
import com.campus.android.domain.models.Card
import io.reactivex.Single

/**
 * Created by Alex Gladkov on 18.08.18.
 * Repository for cards operations
 */
interface CardsRepository {
    fun fetchCards(id: Int, token: String): Single<List<ApiCard>>
    fun addCard(token: String, cardNumber: String, month: Int, year: Int, userId: Int): Single<Any>
    fun fetchCardsFromDB(): Single<List<Card>>
    fun updateStorage(newItems: List<ApiCard>): Single<Boolean>
    fun deleteAll()
    fun deleteCard(token: String, id: Int): Single<Any>
}