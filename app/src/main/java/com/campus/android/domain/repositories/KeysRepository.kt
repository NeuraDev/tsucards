package com.campus.android.domain.repositories

import com.campus.android.domain.models.Key
import io.reactivex.Single

/**
 * Created by Alex Gladkov on 18.08.18.
 * Repository for keys operations
 */
interface KeysRepository {
    fun fetchKeys(token: String, userId: Int): Single<List<Key>>
    fun updateStorage(newItems: List<Key>): Single<Boolean>
    fun addKey(token: String, cardId: Int, userId: Long, gateId: Int, comment: String): Single<Boolean>
    fun deleteAll()
}