package com.campus.android.domain.models

import android.media.Image
import org.joda.time.LocalDate

/**
 * Created by Alex Gladkov on 15.08.18.
 * Data model for key (domain level)
 */
data class Key(val id: Int, val status: Int, val gateId: Int, val cardId: Int, val userId: Int,
               val expired: LocalDate, var title: String)