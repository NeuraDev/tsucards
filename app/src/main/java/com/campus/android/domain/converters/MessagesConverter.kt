package com.campus.android.domain.converters

import com.campus.android.data.models.ApiMessage
import com.campus.android.data.room.models.MessageEntity
import com.campus.android.domain.models.Message

/**
 * Created by Alex Gladkov on 20.08.18.
 * Converter for messages models (data, domain and db layer)
 */
class MessagesConverter {

    fun apiToModel(apiMessage: ApiMessage): Message {
        return Message(id = apiMessage.id, title = apiMessage.title, avatar = "", date = apiMessage.date,
                value = apiMessage.text, type = apiMessage.type.toInt())
    }

    fun dbToModel(messageEntity: MessageEntity): Message {
        return Message(id = messageEntity.id, title = messageEntity.title, avatar = messageEntity.avatar, date = messageEntity.date,
                value = messageEntity.value, type = messageEntity.type)
    }

    fun modelToDB(message: Message): MessageEntity {
        return MessageEntity(id = message.id, title = message.title, avatar = message.avatar, date = message.date,
                value = message.value, type = message.type)
    }
}