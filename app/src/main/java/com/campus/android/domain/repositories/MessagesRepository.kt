package com.campus.android.domain.repositories

import com.campus.android.domain.models.Message
import io.reactivex.Single

/**
 * Created by Alex Gladkov on 18.08.18.
 * Repository for messages operations
 */
interface MessagesRepository {
    fun fetchRemoteMessages(id: Int, token: String): Single<List<Message>>
    fun fetchMessages(): Single<List<Message>>
    fun updateStorage(newItems: List<Message>): Single<Boolean>
    fun deleteAll()
}