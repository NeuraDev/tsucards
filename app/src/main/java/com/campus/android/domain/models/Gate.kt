package com.campus.android.domain.models

import android.os.Parcel
import android.os.Parcelable

data class Gate(val id: Int, val title: String, val place: String, val isTwoFactors: Boolean,
                val avatar: String): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(place)
        parcel.writeByte(if (isTwoFactors) 1 else 0)
        parcel.writeString(avatar)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Gate> {
        override fun createFromParcel(parcel: Parcel): Gate {
            return Gate(parcel)
        }

        override fun newArray(size: Int): Array<Gate?> {
            return arrayOfNulls(size)
        }
    }

}