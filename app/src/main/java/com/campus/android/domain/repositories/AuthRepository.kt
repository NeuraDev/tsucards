package com.campus.android.domain.repositories

import com.campus.android.app.fragments.ProfileFragment
import com.campus.android.data.models.ApiLoginResponse
import com.campus.android.data.models.ApiProfile
import com.campus.android.data.models.ApiProfileSend
import com.campus.android.data.models.ApiProfileWithoutSend
import com.campus.android.domain.models.Configuration
import io.reactivex.Single

/**
 * Created by Alex Gladkov on 18.08.18.
 * Repository for auth operations
 */
interface AuthRepository {
    fun login(email: String, password: String): Single<ApiLoginResponse>
    fun register(email: String, password: String, name: String, surname: String, thirdName: String,
                 date: String, avatar: String, status: String): Single<ApiLoginResponse>
    fun fetchConfiguration(): Single<Configuration>
    fun fetchProfile(token: String, id: Int): Single<ApiProfile>
    fun updateConfiguration(configuration: Configuration): Single<Boolean>
    fun sendFcm(fcmToken: String, token: String): Single<Boolean>
    fun updateProfile(apiProfile: ApiProfileSend, token: String, id: Int): Single<Any>
    fun updateProfile(apiProfile: ApiProfileWithoutSend, token: String, id: Int): Single<Any>
}