package com.campus.android.domain.models

/**
 * Created by admin on 16.08.2018.
 */
data class Card(val id: Int, var title: String, val month: Int, val year: Int,
                val type: String, val bankName: String)