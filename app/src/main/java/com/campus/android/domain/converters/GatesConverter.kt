package com.campus.android.domain.converters

import com.campus.android.data.models.ApiGate
import com.campus.android.data.room.models.GateEntity
import com.campus.android.domain.models.Gate

class GatesConverter {

    fun apiToModel(apiGate: ApiGate): Gate {
        return Gate(id = apiGate.id.toInt(), title = apiGate.name, isTwoFactors = apiGate.twoFactors,
                place = apiGate.place, avatar = apiGate.foto)
    }

    fun modelToDB(gate: Gate): GateEntity {
        return GateEntity(id = gate.id, title = gate.title, isTwoFactors = gate.isTwoFactors, place = gate.place)
    }

    fun dbToModel(gateEntity: GateEntity): Gate {
        return Gate(id = gateEntity.id, title = gateEntity.title, isTwoFactors = gateEntity.isTwoFactors,
                place = gateEntity.place, avatar = "")
    }
}