package com.campus.android.domain.converters

import com.campus.android.data.models.ApiKey
import com.campus.android.data.room.models.KeyEntity
import com.campus.android.domain.models.Key
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

class KeysConverter {

    fun apiToModel(apiKey: ApiKey): Key {
        val dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss")
        return Key(id = apiKey.id.toInt(), status = apiKey.status.toInt(), gateId = apiKey.gateId.toInt(),
                cardId = apiKey.cardid.toInt(), expired = LocalDate.parse(apiKey.expired, dateFormat), userId = apiKey.userId.toInt(),
                title = "")
    }

    fun modelToDB(key: Key): KeyEntity {
        return KeyEntity(id = key.id, status = key.status, gateId = key.gateId, cardId = key.cardId,
                userId = key.userId, expired = key.expired.toString("dd.MM.yyyy HH:mm:ss"))
    }

    fun dbToModel(keyEntity: KeyEntity): Key {
        val dateFormatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss")
        return Key(id = keyEntity.id, status = keyEntity.status, gateId = keyEntity.gateId, cardId = keyEntity.cardId,
                userId = keyEntity.userId, expired = LocalDate.parse(keyEntity.expired, dateFormatter),
                title = "")
    }
}