package com.campus.android.domain.converters

import com.campus.android.data.room.models.ConfigEntity
import com.campus.android.domain.models.Configuration

/**
 * Created by agladkov on 12.02.18.
 */
class ConfigModelConverter {
    private val TAG = ConfigModelConverter::class.java.simpleName

    fun dbToModel(configEntity: ConfigEntity): Configuration {
        return Configuration(id = configEntity.userId, name = configEntity.name,
                surname = configEntity.surname, email = configEntity.email,
                token = configEntity.token, fcmToken = configEntity.fcmToken, avatar = configEntity.avatar,
                patronymic = configEntity.patronymic, birthDay = configEntity.birthDay,
                status = configEntity.status)
    }

    fun modelToDb(configurationModel: Configuration): ConfigEntity {
        return ConfigEntity(userId = configurationModel.id, name = configurationModel.name,
                surname = configurationModel.surname, email = configurationModel.email,
                token = configurationModel.token, fcmToken = configurationModel.fcmToken, avatar = configurationModel.avatar,
                patronymic = configurationModel.patronymic, birthDay = configurationModel.birthDay, id = 0,
                status = configurationModel.status)
    }
}