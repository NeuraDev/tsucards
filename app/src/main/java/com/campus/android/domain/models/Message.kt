package com.campus.android.domain.models

/**
 * Created by Nugaev Marat on 16.08.2018.
 * Data model for kcard (domain level)
 */

data class Message(val id: String, val title: String, val value: String, val date: String, val avatar: String,
                   val type: Int)