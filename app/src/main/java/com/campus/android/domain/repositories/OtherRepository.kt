package com.campus.android.domain.repositories

import io.reactivex.Single
import java.io.File

/**
 * Created by Alex Gladkov on 20.08.18.
 * Repository for other operations
 */
interface OtherRepository {
    fun uploadFile(file: File): Single<String>
}