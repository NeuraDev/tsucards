package com.campus.android.domain.models

import android.arch.persistence.room.PrimaryKey
import android.util.Log
import com.campus.android.app.presenters.MainPresenter
import java.io.Serializable

/**
 * Created by agladkov on 12.02.18.
 */
data class Configuration(var id: Long, var token: String, var avatar: String?,
                         var name: String?, var surname: String?, var patronymic: String?,
                         var birthDay: String?, var fcmToken: String?, var email: String?,
                         var status: String): Serializable {

    companion object {
        private val TAG: String = MainPresenter::class.java.simpleName
        fun defaultInstance(): Configuration {
            return Configuration(id = 0, name = "", surname = "", patronymic = "", token = "", avatar = "",
                    birthDay = "", fcmToken = "", email = "", status = "")
        }

        fun createFromFcm(fcmToken: String): Configuration {
            return Configuration(id = 0, name = "", surname = "", patronymic = "", token = "", avatar = "",
                    birthDay = "", fcmToken = fcmToken, email = "", status = "")
        }
    }
}