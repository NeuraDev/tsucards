package com.campus.android.domain.repositories

import com.campus.android.data.models.ApiCard
import com.campus.android.domain.models.Card
import com.campus.android.domain.models.Gate
import io.reactivex.Single

/**
 * Created by Alex Gladkov on 21.08.18.
 * Repository for objects operations
 */
interface ReaderRepository {
    fun fetchReaders(id: Int, token: String):  Single<List<Gate>>
//    fun addCard(token: String, cardNumber: String, month: Int, year: Int, userId: Int): Single<Any>
//    fun fetchCardsFromDB(): Single<List<Card>>
//    fun updateStorage(newItems: List<ApiCard>): Single<Boolean>
}